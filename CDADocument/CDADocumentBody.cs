﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Тело документа
    public class CDADocumentBody : ICDASerializable
    {
        private List<CDADocumentSection> sections;

        public CDADocumentBody(List<CDADocumentSection> sections)
        {
            this.sections = sections;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elBody = document.CreateElement("structuredBody");
            foreach (CDADocumentSection section in sections)
                elBody.AppendChild(new CDAContainer("component", section).ToXmlElement(document));
            return elBody;
        }
    }
}
