﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Измерение витального параметра
    public class CDAVitalParameterMeasuring : ICDASerializable
    {
        private CDACodifCode statusCode;
        private CDATime time;
        private string name;
        private List<CDAVitalParameter> vitalParameters;

        public string Time
        { get { return time.GetDate() + "<br/>" + time.GetTime(); } }

        public string Name
        { get { return name; } }

        public string Unit
        { get { return vitalParameters.First().Value.Unit; } }

        public List<CDAVitalParameter> VitalParameters
        { get { return vitalParameters; } }

        public CDAVitalParameterMeasuring(string name, CDACodifCode statusCode, CDATime time, params CDAVitalParameter[] parameters)
        {
            this.name = name;
            this.statusCode = statusCode;
            this.time = time;
            vitalParameters = new List<CDAVitalParameter>();
            foreach (CDAVitalParameter parameter in parameters)
                vitalParameters.Add(parameter);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(statusCode);
            elements.Add(time);
            foreach (CDAVitalParameter vitalParameter in vitalParameters)
                elements.Add(vitalParameter);
            CDAContainer organizer = new CDAContainer("organizer", elements, new CDAElementAttribute("classCode", "CLUSTER"), new CDAElementAttribute("moodCode", "EVN"));
            return organizer.ToXmlElement(document);
        }

        public static CDAVitalParameterMeasuring CreatePressure()
        {
            return new CDAVitalParameterMeasuring(
                "Артериальное давление",
                new CDACodifCode("statusCode", "completed", null, null, null),
                new CDATime(new DateTime(2016, 08, 08, 10, 10, 00, DateTimeKind.Local)),
                new CDAVitalParameter(
                    new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1010", "Справочник витальных параметров", "Давление систолическое"),
                    new CDAValue(new CDAQuantity("135", "мм.рт.ст."))),
                new CDAVitalParameter(
                    new CDACodifCode("2", "1.2.643.5.1.13.13.11.1010", "Справочник витальных параметров", "Давление диастолическое"),
                    new CDAValue(new CDAQuantity("85", "мм.рт.ст."))));
        }

        public static CDAVitalParameterMeasuring CreatePulse()
        {
            return new CDAVitalParameterMeasuring(
                "Частота сердечных сокращений",
                new CDACodifCode("statusCode", "completed", null, null, null),
                new CDATime(new DateTime(2016, 08, 08, 11, 11, 00, DateTimeKind.Local)),
                new CDAVitalParameter(
                    new CDACodifCode("code", "4", "1.2.643.5.1.13.13.11.1010", "Справочник витальных параметров", "Частота сердечных сокращений"),
                    new CDAValue(new CDAQuantity("77", "уд./мин"))));
        }

        public static CDAVitalParameterMeasuring CreateBreathRate()
        {
            return new CDAVitalParameterMeasuring(
                "Частота дыхания",
                new CDACodifCode("statusCode", "completed", null, null, null),
                new CDATime(new DateTime(2016, 08, 08, 11, 11, 00, DateTimeKind.Local)),
                new CDAVitalParameter(
                    new CDACodifCode("code", "7", "1.2.643.5.1.13.13.11.1010", "Справочник витальных параметров", "Частота дыхания"),
                    new CDAValue(new CDAQuantity("23", "1/мин"))));
        }
    }
}
