﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDADocumentClasses
{
    // Назначение лекарства
    public class CDADrugAdministration
    {
        private CDADrug drug; // лекарство
        private CDAText scheme; // схема лечения
        private CDATime time; // время приема

        public CDADrug Drug
        { get { return drug; } }

        public string Name
        { get { return drug.Name; } }

        public string Scheme
        { get { return scheme.Value; } }

        public string Time
        { get { return time.ToString(); } }

        public CDADrugAdministration(CDADrug drug, CDAText scheme, CDATime time)
        {
            this.drug = drug;
            this.scheme = scheme;
            this.time = time;
        }
    }
}
