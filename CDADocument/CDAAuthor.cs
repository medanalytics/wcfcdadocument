﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Информация об авторе документа
    public class CDAAuthor : ICDASerializable
    {
        private CDATime SignDate; // Время подписания
        private CDAEmployeeInfo Author;

        public CDAAuthor(CDAEmployeeInfo Author, CDATime SignDate)
        {
            this.SignDate = SignDate;
            this.Author = Author;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elAuthor = document.CreateElement("author");
            // время подписания            
            elAuthor.AppendChild(SignDate.ToXmlElement(document));
            // автор
            XmlElement elAssigned = document.CreateElement("assignedAuthor");
            foreach (XmlElement element in Author.ToXmlElements(document))
                elAssigned.AppendChild(element);

            elAuthor.AppendChild(elAssigned);

            return elAuthor;
        }

        public static CDAAuthor Create()
        {
            CDATime SignDate = new CDATime(new DateTime(2017, 08, 31, 15, 35, 00, DateTimeKind.Local), "time");
            CDAEmployeeInfo Author = CDAEmployeeInfo.Create("2341");
            return new CDAAuthor(Author, SignDate);
        }
    }
}
