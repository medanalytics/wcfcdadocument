﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // атрибут элемента
    public class CDAElementAttribute
    {
        private string Name;
        private string Value;

        public CDAElementAttribute(string Name, string Value)
        {
            this.Name = Name;
            this.Value = Value;
        }

        public XmlElement ToXmlElementAtr(XmlElement element)
        {
            element.SetAttribute(Name, Value);
            return element;
        }
    }
}
