﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Референтный интервал
    public class CDAReferenceRange : ICDASerializable
    {
        private CDAText text;
        private CDAValue value;
        private CDACodifCode interpretation;

        public string Range
        { get { return text.Value; } }

        public CDAReferenceRange(CDAText text, CDAValue value, CDACodifCode interpretation)
        {
            this.text = text;
            this.value = value;
            this.interpretation = interpretation;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elRange = document.CreateElement("referenceRange");
            XmlElement elObsRange = document.CreateElement("observationRange");
            elObsRange.AppendChild(text.ToXmlElement(document));
            elObsRange.AppendChild(value.ToXmlElement(document));
            elObsRange.AppendChild(interpretation.ToXmlElementAtr(document.CreateElement("interpretationCode")));
            elRange.AppendChild(elObsRange);

            return elRange;
        }
    }
}
