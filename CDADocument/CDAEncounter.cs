﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Случай получения мед. помощи для секций документа
    public class CDAEncounter : ICDASerializable
    {
        private CDARootExt id;
        private CDATime time;
        private CDACodifCode order; // порядок обращения
        private CDACodifCode diagnosis;

        public string Diagnosis
        { get { return diagnosis.GetCode(); } }

        public string Order
        { get { return order.GetDisplayName(); } }

        public string Period
        { get { return time.ToString(); } }

        public CDAEncounter(CDARootExt id, CDATime time, CDACodifCode order, CDACodifCode diagnosis)
        {
            this.id = id;
            this.time = time;
            this.order = order;
            this.diagnosis = diagnosis;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(id);
            elements.Add(time);
            elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateObservation(order),
                                        new CDAElementAttribute("typeCode", "SUBJ"), new CDAElementAttribute("inversionInd", "true")));
            elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateObservation(diagnosis),
                                        new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            CDAActStatement observation = CDAActStatement.CreateEncounter(elements);
            return observation.ToXmlElement(document);
        }

        public static CDAEncounter Create()
        {
            // Идентификатор случая
            CDARootExt id = new CDARootExt("id", "1.2.643.5.1.13.3.25.77.231.100.1.1.15", "908964234678");
            // Период
            CDATime time = new CDATime(new DateTime(2016, 08, 08, 07, 07, 00, DateTimeKind.Local), new DateTime(2016, 10, 10, 09, 09, 00, DateTimeKind.Local));
            // Порядок обращения
            CDACodifCode order = new CDACodifCode("code", "2", "1.2.643.5.1.13.13.11.1007", "Порядок случаев госпитализации или обращения", "Повторный");
            // КодМКБ
            CDACodifCode diag = new CDACodifCode("code", "T23.2", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Термический ожог запястья и кисти второй степени");

            return new CDAEncounter(id, time, order, diag);
        }
    }
}
