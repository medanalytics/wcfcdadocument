﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDATextParagraph
    {
        private XmlElement elParagraph;
        private XmlDocument document;

        public CDATextParagraph(XmlElement elParagraph, XmlDocument document)
        {
            this.elParagraph = elParagraph;
            this.document = document;
        }

        public void AddCaption(string caption)
        {
            XmlElement elCaption = document.CreateElement("caption");
            elCaption.InnerXml = caption;
            elParagraph.AppendChild(elCaption);
        }

        public void AddXmlElement(XmlElement element)
        {
            elParagraph.AppendChild(element);
        }
    }
}
