﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Лабораторный показатель
    class CDALabFactor : ICDASerializable
    {
        private CDACodifCode code;
        private CDACodifCode statusCode;
        private CDATime time;
        private CDAValue value;
        private CDACodifCode interpretationCode;
        private CDASpecimen specimen;
        private List<CDAPerformer> performers;
        private List<CDADevice> equipment;
        private CDAReferenceRange referenceRange;

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Value
        { get { return value.Value; } }

        public string Unit
        { get { return value.Unit; } }

        public string Range
        { get { return referenceRange.Range; } }

        public string Equipment
        {
            get
            {
                string result = "";
                foreach (CDADevice equip in equipment)
                {
                    if (result != "")
                        result = result + "<br/>";
                    result = result + equip.Name;
                }

                return result;
            }
        }

        public string Performer
        {
            get
            {
                string result = "";
                foreach (CDAPerformer performer in performers)
                {
                    if (result != "")
                        result = result + "<br/>";
                    result = result + performer.GetPerformer();
                }

                return result;
            }
        }

        public string Date
        { get { return time.ToString(); } }

        public CDALabFactor(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDAValue value,
                            CDACodifCode interpretationCode, CDASpecimen specimen, List<CDAPerformer> performers,
                            List<CDADevice> equipment, CDAReferenceRange referenceRange)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.value = value;
            this.interpretationCode = interpretationCode;
            this.specimen = specimen;
            this.performers = performers;
            this.equipment = equipment;
            this.referenceRange = referenceRange;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> lstFactor = new List<ICDASerializable>();
            lstFactor.Add(code);
            lstFactor.Add(statusCode);
            lstFactor.Add(time);
            lstFactor.Add(value);
            lstFactor.Add(interpretationCode);
            lstFactor.Add(specimen);
            foreach (CDAPerformer performer in performers)
                lstFactor.Add(performer);
            foreach (CDADevice equip in equipment)
                lstFactor.Add(equip);
            if (referenceRange != null)
                lstFactor.Add(referenceRange);

            CDAActStatement factor = CDAActStatement.CreateObservation(lstFactor);

            return factor.ToXmlElement(document);
        }

        public static CDALabFactor Create(string id)
        {
            // Гемоглобин
            CDACodifCode hCode = new CDACodifCode("code", "1017128", "1.2.643.5.1.13.13.11.1080", "Федеральный справочник лабораторных исследований. Справочник лабораторных тестов.", "Гемоглобин общий, массовая концентрация в крови");
            CDACodifCode hStatusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime hTime = new CDATime(new DateTime(2016, 09, 02, 11, 11, 00, DateTimeKind.Local));
            CDAValue hValue = new CDAValue(new CDAQuantity("166", "г/л"));
            CDACodifCode hInterpretationCode = new CDACodifCode("interpretationCode", "H", null, null, null);
            CDASpecimen hSpecimen = CDASpecimen.Create("124562156");
            List<CDAPerformer> hPerformers = new List<CDAPerformer>();
            hPerformers.Add(new CDAPerformer(CDAEmployeeInfo.CreateShort("2341"), null));
            List<CDADevice> hEquipment = new List<CDADevice>();
            hEquipment.Add(new CDADevice(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.231.100.1.1.67", "1234"), "Гематологический анализатор Sysmex KX21"));
            CDAReferenceRange hRange = new CDAReferenceRange(new CDAText("120 - 150 г/л"),
                            new CDAValue(new CDAQuantity("120", "г/л"), new CDAQuantity("150", "г/л")),
                            new CDACodifCode("interpretationCode", "N", null, null, null));

            return new CDALabFactor(hCode, hStatusCode, hTime, hValue, hInterpretationCode, hSpecimen, hPerformers, hEquipment, hRange);
        }
    }
}
