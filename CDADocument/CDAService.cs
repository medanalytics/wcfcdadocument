﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Медицинская услуга
    public class CDAService : ICDASerializable
    {
        private CDACodifCode code;
        private CDATime time;

        public string Code
        { get { return code.GetCode(); } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Date
        { get { return time.GetDate(); } }

        public CDAService(CDACodifCode code, CDATime time)
        {
            this.code = code;
            this.time = time;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAActStatement act = CDAActStatement.CreateAct(code, time);
            return act.ToXmlElement(document);
        }
    }
}
