﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Организация - получатель документа
    public class CDARecipient : ICDASerializable
    {
        private CDAOrganisation Recipient;

        public CDARecipient(CDAOrganisation Recipient)
        {
            this.Recipient = Recipient;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elRecipient = document.CreateElement("informationRecipient");
            XmlElement elIntended = document.CreateElement("intendedRecipient");
            XmlElement elRecieved = document.CreateElement("receivedOrganization");
            foreach (XmlElement element in Recipient.ToXmlElements(document))
                elRecieved.AppendChild(element);
            elIntended.AppendChild(elRecieved);
            elRecipient.AppendChild(elIntended);

            return elRecipient;
        }
    }
}
