﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Адрес
    public class CDAAddress : ICDASerializable
    {
        // Вид адреса
        public enum Kind
        { Unspecified, H, HP }

        // Строка с адресом или значене nullFlavor
        private CDAString AddressLine;
        // Регион
        private CDAString State;
        // Тип адреса (регистрации, фактического проживания)
        private Kind Use;
        // Значение nullFlavor
        private nullFlavor nF;

        public CDAAddress(CDAString AddressLine, CDAString State, Kind Use)
        {
            this.AddressLine = AddressLine;
            this.State = State;
            this.Use = Use;
            this.nF = null;
        }

        public CDAAddress(CDAString AddressLine, CDAString State)
        {
            this.AddressLine = AddressLine;
            this.State = State;
            this.Use = Kind.Unspecified;
            this.nF = null;
        }

        public CDAAddress(nullFlavor nF)
        {
            this.nF = nF;
        }

        // Создаем XmlElement, содержащий данные адреса
        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elAddr = document.CreateElement("addr");
            // проверим на nullFlavor
            if (nF != null)
                elAddr = nF.ToXmlElementAtr(elAddr);
            else
            {
                if (Use != Kind.Unspecified)
                    elAddr.SetAttribute("use", Use.ToString());
                // Строка адреса
                XmlElement elAddressLine = AddressLine.ToXmlElementVal(document.CreateElement("streetAddressLine"));
                elAddr.AppendChild(elAddressLine);
                // Регион
                XmlElement elState = State.ToXmlElementVal(document.CreateElement("state"));
                elAddr.AppendChild(elState);
            }

            return elAddr;
        }
    }
}
