﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Observation с элементами code и value
    public class CDACodeVal : ICDASerializable
    {
        private CDACodifCode code;
        private CDAValue value;
        private CDATextReference reference;

        public CDACodifCode Code
        { get { return code; } }

        public CDAValue Value
        { get { return value; } }

        public CDACodeVal(CDACodifCode code, CDAValue value)
        {
            this.code = code;
            this.value = value;
            reference = null;
        }

        public CDACodeVal(CDACodifCode code)
        {
            this.code = code;
            value = null;
            reference = null;
        }

        public void SetReference(CDATextReference reference)
        {
            this.reference = reference;
            reference.Name = "text";
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            if (code != null)
                elements.Add(code);
            if (reference != null)
                elements.Add(reference);
            if (value != null)
                elements.Add(value);

            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }
    }
}
