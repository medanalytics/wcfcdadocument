﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Льготная категория
    public class CDACodeTime : ICDASerializable
    {
        private CDACodifCode code;
        private CDATime time;

        public string Name
        { get { return code.GetDisplayName(); } }

        public string LowDate
        { get { return time.Low.ToString("dd.MM.yyyy"); } }

        public string HighDate
        { get { return time.High.ToString("dd.MM.yyyy"); } }

        public CDACodeTime(CDACodifCode code, CDATime time)
        {
            this.code = code;
            this.time = time;
        }

        public void SetCodeReference(CDATextReference reference)
        {
            code.SetReference(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            return CDAActStatement.CreateObservation(code, time).ToXmlElement(document);
        }
    }
}
