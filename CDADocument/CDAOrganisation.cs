﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Организация
    public class CDAOrganisation
    {
        // Идентификатор организации
        private CDARootExt Id;
        // Наименование организации
        public CDAString Name;
        // Список контактов
        public List<CDATelecom> Contacts;
        // Адрес
        public CDAAddress Address;

        public CDAOrganisation(CDARootExt Id, CDAString Name, CDAAddress Address, List<CDATelecom> Contacts)
        {
            this.Id = Id;
            this.Name = Name;
            this.Contacts = Contacts;
            this.Address = Address;
        }

        public List<XmlElement> ToXmlElements(XmlDocument document)
        {
            List<XmlElement> lstElOrg = new List<XmlElement>();
            // id            
            lstElOrg.Add(Id.ToXmlElementAtr(document.CreateElement("id")));
            // название            
            lstElOrg.Add(Name.ToXmlElementVal(document.CreateElement("name")));
            // контакты
            foreach (CDATelecom contact in Contacts)
                lstElOrg.Add(contact.ToXmlElement(document));
            // Адрес
            if (Address != null)
                lstElOrg.Add(Address.ToXmlElement(document));

            return lstElOrg;
        }

        // Создаем объект CDAOrganisation по id медицинской организации
        public static CDAOrganisation CreateMO(string id)
        {
            CDARootExt Id = new CDARootExt(id, null);
            List<CDATelecom> Contacts = new List<CDATelecom>();
            Contacts.Add(new CDATelecom("tel:+74955360200", CDATelecom.Kind.WP));
            Contacts.Add(new CDATelecom("fax:+74955360123", CDATelecom.Kind.WP));
            Contacts.Add(new CDATelecom("http://od2.onco62.ru/"));
            CDAAddress Address = new CDAAddress(new CDAString("125130, г. Москва, Старопетровский пр., д. 6"), new CDAString("77"));
            return new CDAOrganisation(Id: Id, Name: new CDAString("Филиал ГАУЗ \"МГОБ №62 ДЗМ\", ОД2"),
                                        Address: Address, Contacts: Contacts);
        }

        // Создаем организацию-владельца документа. Ее отличие в том, что мы используем только
        // один основной телефон.
        public static CDAOrganisation CreateCustodian(string id)
        {
            CDARootExt Id = new CDARootExt(id, null);
            List<CDATelecom> Contacts = new List<CDATelecom>();
            Contacts.Add(new CDATelecom("tel:+74955360200", CDATelecom.Kind.WP));
            CDAAddress Address = new CDAAddress(new CDAString("125130, г. Москва, Старопетровский пр., д. 6"), new CDAString("77"));
            return new CDAOrganisation(Id: Id, Name: new CDAString("Филиал ГАУЗ \"МГОБ №62 ДЗМ\", ОД2"),
                                        Address: Address, Contacts: Contacts);
        }

        // Создаем объект CDAOrganisation по id страховой компании
        public static CDAOrganisation CreateSMO(string id)
        {
            CDARootExt Id = new CDARootExt("1.2.643.5.1.13.2.1.1.635", id);
            List<CDATelecom> Contacts = new List<CDATelecom>();
            Contacts.Add(new CDATelecom("tel:+78612686644"));
            Contacts.Add(new CDATelecom("fax:+78612686644", CDATelecom.Kind.WP));
            Contacts.Add(new CDATelecom("http://http://www.msk-med.ru/"));
            CDAAddress Address = new CDAAddress(new CDAString("103132, г. Москва, Старая пл., д. 2"), new CDAString("77"));
            return new CDAOrganisation(Id: Id, Name: new CDAString("ООО \"МСК-МЕД\""),
                                        Address: Address, Contacts: Contacts);
        }
    }
}
