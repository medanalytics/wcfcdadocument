﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Инструментальное исследование
    public class CDAObservationInstr : ICDASerializable
    {
        private CDACodifCode code;
        private CDACodifCode statusCode;
        private CDATime time;
        private CDACodifCode priorityCode;
        private CDAValue value;
        private CDAPerformer performer;
        private List<CDAService> services; // медицинская услуга
        private CDAExternalDocReference reference;
        private List<CDAObservationComponent> components;

        public string Date
        { get { return time.GetDate(); } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Result
        { get { return value.Value; } }

        public string Priority
        { get { return priorityCode.GetDisplayName(); } }

        public string Performer
        { get { return performer.GetPerformer(); } }

        // Конструктор для создания исследования в стационаре
        public CDAObservationInstr(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, CDAExternalDocReference reference, params CDAObservationComponent[] components)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;            
            this.reference = reference;
            this.services = new List<CDAService>();
            this.components = new List<CDAObservationComponent>();
            foreach (CDAObservationComponent component in components)
                this.components.Add(component);
        }

        // Конструктор для создания исследования в амбулаторных условиях
        public CDAObservationInstr(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, CDAExternalDocReference reference, params CDAService[] services)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;            
            this.reference = reference;
            this.components = new List<CDAObservationComponent>();
            this.services = new List<CDAService>();
            foreach (CDAService service in services)
                this.services.Add(service);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(code);
            elements.Add(statusCode);
            elements.Add(time);
            elements.Add(priorityCode);
            elements.Add(value);
            elements.Add(performer);
            foreach (CDAService service in services)
                elements.Add(new CDAContainer("entryRelationship", service, new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            foreach (CDAObservationComponent component in components)
                elements.Add(new CDAContainer("entryRelationship", "COMP", component));
            if (reference != null)
                elements.Add(reference);
            
            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }

        // создаем исследование, проходившее в стационаре
        public static CDAObservationInstr CreateStac()
        {
            // код исследования
            CDACodifCode code = new CDACodifCode("code", "1", "1.2.643.5.1.13.2.1.1.1504.11", "Справочник типов инструментальных исследований", "Рентген");            
            // статус
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            // время исследования
            CDATime time = new CDATime(new DateTime(2014, 05, 15, 14, 30, 0, DateTimeKind.Local));            
            // приоритет
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");
            // текст результатов
            CDAValue value = new CDAValue(@"Результаты и\или заключение");
            // исполнитель
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));            
            // Компонент исследования
            CDAObservationComponent component = new CDAObservationComponent(            
                    new CDACodifCode("code", "A06.03.036", "1.2.643.5.1.13.2.1.1.473", "Номенклатура медицинских услуг", "Рентгенография нижней конечности"),
                    new CDACodifCode("statusCode", "completed", null, null, null));
            // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754871"));

            return new CDAObservationInstr(code: code,
                                                  statusCode: statusCode,
                                                  time: time,
                                                  priorityCode: priorityCode,
                                                  value: value,
                                                  performer: performer,                                                  
                                                  reference: reference,
                                                  components: component);
        }

        // создаем исследование, проходившее в амбулатории
        public static CDAObservationInstr CreateAmb()
        {
            // код исследования
            CDACodifCode code = new CDACodifCode("code", "50", "1.2.643.5.1.13.2.1.1.1504.11", "Справочник типов инструментальных исследований", "ЭКГ");
            // статус
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            // время исследования
            CDATime time = new CDATime(new DateTime(2016, 08, 20, 08, 08, 0, DateTimeKind.Local));
            // приоритет
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");
            // текст результатов
            CDAValue value = new CDAValue(@"Длительность (час.): 18:34.Каналы – 3.Всего зарегистрировано комплексов: 60205<br/>1/. Ритм синусовый со средней ЧСС 53 в мин. (макс. ЧСС 92 в мин. 2:56; мин. ЧСС 42 в мин. 0:03 - сон).<br/>2/. За время исследования больного паузы продолжительностью больше 2.0 сек. не зафиксированы. Максимальный RR 1.86 сек. (1:50).<br/>3/. На протяжении исследования отмечалась А-В блокада II cтепени с PQ 0.26-0.28 сек.");
            // исполнитель
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));
            // медицинские услуги            
            CDAService service = new CDAService(
                        new CDACodifCode("code", "A05.10.008.001", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Холтеровское мониторирование сердечного ритма (ХМ-ЭКГ)"),
                        new CDATime(new DateTime(2014, 05, 15, 14, 30, 0, DateTimeKind.Local)));
            // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754871"));

            return new CDAObservationInstr(code: code,
                                                  statusCode: statusCode,
                                                  time: time,
                                                  priorityCode: priorityCode,
                                                  value: value,
                                                  performer: performer,
                                                  reference: reference,
                                                  services: service);
        }
    }
}
