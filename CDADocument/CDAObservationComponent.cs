﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAObservationComponent : ICDASerializable
    {
        CDACodifCode code;
        CDACodifCode statusCode;

        public CDAObservationComponent(CDACodifCode code, CDACodifCode statusCode)
        {
            this.code = code;
            this.statusCode = statusCode;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            return CDAActStatement.CreateObservation(code, statusCode).ToXmlElement(document);
        }
    }
}
