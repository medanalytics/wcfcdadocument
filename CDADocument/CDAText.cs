﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Текстовое наполнение секции документа
    public class CDAText : ICDASerializable
    {
        private XmlDocument Text;
        private XmlElement root;
        private string rootName;

        public string Value
        { get { return root.InnerXml; } }

        public CDAText(XmlDocument document)
        {
            XmlElement root;
            rootName = "text";
            // Создадим новый документ, чтобы не изменить передаваемый в качестве параметра
            Text = new XmlDocument();
            if (document.SelectSingleNode(rootName) != null)
            {
                XmlNode nodeText = Text.ImportNode(document.SelectSingleNode(rootName), true);
                root = nodeText as XmlElement;
            }
            else
                root = Text.CreateElement(rootName);

            Text.AppendChild(root);
        }

        public CDAText()
        {
            rootName = "text";
            Text = new XmlDocument();
            root = Text.CreateElement(rootName);
            Text.AppendChild(root);
        }

        public CDAText(string content)
        {
            rootName = "text";
            Text = new XmlDocument();
            root = Text.CreateElement(rootName);
            root.InnerXml = content;
            Text.AppendChild(root);
        }

        public CDAText(string content, string rootName)
        {
            if (rootName != null)
                this.rootName = rootName;
            else
                this.rootName = "text";
            Text = new XmlDocument();
            root = Text.CreateElement(rootName);
            root.InnerXml = content;
            Text.AppendChild(root);
        }

        public CDATextTable AddTable(string caption, params int[] ColWidth)
        {
            XmlElement elTable = Text.CreateElement("table");
            elTable.SetAttribute("width", "100%");
            root.AppendChild(elTable);

            return new CDATextTable(elTable, Text, caption, ColWidth);
        }

        public CDATextTable AddTable(int numCol)
        {
            XmlElement elTable = Text.CreateElement("table");
            root.AppendChild(elTable);

            return new CDATextTable(elTable, Text, numCol);
        }

        public CDATextTable AddTable(params string[] Columns)
        {
            XmlElement elTable = Text.CreateElement("table");
            root.AppendChild(elTable);

            return new CDATextTable(elTable, Text, Columns);
        }

        public void AddBr()
        {
            root.AppendChild(Text.CreateElement("br"));
        }

        public CDATextParagraph AddParagraph()
        {
            XmlElement elParagraph = Text.CreateElement("paragraph");
            root.AppendChild(elParagraph);
            return new CDATextParagraph(elParagraph, Text);
        }

        public void AddParagraph(string paragraph)
        {
            XmlElement elParagraph = Text.CreateElement("paragraph");
            elParagraph.InnerXml = paragraph;
            root.AppendChild(elParagraph);
        }

        public CDATextList AddList(string Caption, params CDAElementAttribute[] Attributes)
        {
            XmlElement elList = Text.CreateElement("list");
            root.AppendChild(elList);

            return new CDATextList(elList, Text, Caption, Attributes);
        }

        public XmlElement CreateTextWithReference(CDATextReference reference, string text)
        {
            XmlElement element = Text.CreateElement("content");
            element.SetAttribute("ID", reference.Value);
            element.InnerXml = text;
            return element;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elText = document.CreateElement(rootName);
            if (Text.SelectSingleNode(rootName) != null)
            {
                XmlNode nodeText = document.ImportNode(Text.SelectSingleNode(rootName), true);
                elText = nodeText as XmlElement;
            }
            return elText;
        }
    }
}
