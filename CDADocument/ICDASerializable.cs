﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // интерфейс, реализующий возможность преобразования в XmlElement
    public interface ICDASerializable
    {
        XmlElement ToXmlElement(XmlDocument document);
    }
}
