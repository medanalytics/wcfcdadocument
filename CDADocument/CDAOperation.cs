﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Хирургическая операция
    public class CDAOperation : ICDASerializable
    {
        private CDACodifCode code;
        private CDAText text;
        private CDACodifCode statusCode;
        private CDATime time;
        private List<CDAPerformer> performers;
        private List<CDAEquipment> lstEquipment; // список использованной оапаратуры
        private List<CDAManufacturedProduct> manProducts; // список устройств и имплантов
        private CDAAnesthetic anesthetic; // использованная анестезия
        private CDAExternalDocReference reference;

        public string Date
        { get { return time.GetDate(); } }

        public string Name
        { 
            get 
            { 
                string val = code.GetDisplayName();
                foreach (CDAManufacturedProduct product in manProducts)
                    val = CDAString.ConcatWithSeparator(val, "установлено: " + product.Code.GetDisplayName(), "<br/>");
                return val;
            } 
        }

        public string Code
        { get { return code.GetCode(); } }

        public string Anesthesy
        { get { return anesthetic.Code.GetDisplayName(); } }

        public string Performer
        {
            get
            {
                string val = "";
                foreach (CDAPerformer performer in performers)
                    val = CDAString.ConcatWithSeparator(val, performer.GetPerformer(), "<br/><br/>");
                return val;
            }
        }
            
        public CDAOperation(CDACodifCode code, CDAText text, CDACodifCode statusCode, CDATime time, List<CDAPerformer> performers,
                            List<CDAEquipment> lstEquipment, List<CDAManufacturedProduct> manProducts, CDAAnesthetic anesthetic, CDAExternalDocReference reference)
        {
            this.code = code;
            this.text = text;
            this.statusCode = statusCode;
            this.time = time;
            this.performers = performers;
            this.lstEquipment = lstEquipment;
            this.manProducts = manProducts;
            this.anesthetic = anesthetic;
            this.reference = reference;
        }

        public CDAOperation(CDACodifCode code, CDACodifCode statusCode, CDATime time, List<CDAPerformer> performers, CDAExternalDocReference reference)
        {
            this.code = code;
            this.text = null;
            this.statusCode = statusCode;
            this.time = time;
            this.performers = performers;
            this.lstEquipment = new List<CDAEquipment>();
            this.manProducts = new List<CDAManufacturedProduct>();
            this.anesthetic = null;
            this.reference = reference;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            if (code != null)
                elements.Add(code);
            if (statusCode != null)
                elements.Add(statusCode);
            if (time != null)
                elements.Add(time);
            foreach (CDAPerformer performer in performers)
                elements.Add(performer);
            foreach (CDAEquipment equipment in lstEquipment)
                elements.Add(equipment);
            foreach (CDAManufacturedProduct product in manProducts)
                elements.Add(product);
            if (anesthetic != null)
                elements.Add(anesthetic);
            if (reference != null)
                elements.Add(reference);

            return CDAActStatement.CreateProcedure(elements).ToXmlElement(document);
        }

        public static CDAOperation CreateStac()
        {
            CDACodifCode code =  new CDACodifCode("code", "A11.03.001", "1.2.643.5.1.13.2.1.1.473", "Номенклатура медицинских услуг", "Биопсия кости");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2014, 05, 15, 11, 21, 0, DateTimeKind.Local));
            List<CDAPerformer> performers = new List<CDAPerformer>();
            performers.Add(new CDAPerformer(CDAEmployeeInfo.CreateShort("2343")));
            CDAExternalDocReference reference = new CDAExternalDocReference(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754858"));                

            return new CDAOperation(code, statusCode, time, performers, reference);
        }

        public static CDAOperation CreateAmb()
        {
            CDACodifCode code = new CDACodifCode("code", "A16.01.004", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Хирургическая обработка раны или инфицированной ткани");
            CDAText text = new CDAText("Хирургическая обработка раны или инфицированной ткани");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2016, 10, 02, 14, 10, 0, DateTimeKind.Local));
            List<CDAPerformer> performers = new List<CDAPerformer>();
            // добавим исполнителя, в должность которого добавлена информация о специальности
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343").AddSpeciality());
            performers.Add(new CDAPerformer(CDAEmployeeInfo.CreateShort("2343").AddSpeciality()));
            // список аппаратуры
            List<CDAEquipment> lstEquipment = new List<CDAEquipment>();
            lstEquipment.Add(new CDAEquipment(new CDACodifCode("code", "4", "1.2.643.5.1.13.13.11.1048", "Учетные группы аппаратуры, используемой при операциях", "Криогенная")));
            // список устройств
            List<CDAManufacturedProduct> manProducts = new List<CDAManufacturedProduct>();
            manProducts.Add(new CDAManufacturedProduct(new CDACodifCode("code", "28", "1.2.643.5.1.13.13.11.1079", "Виды имплантируемых медицинских изделий и вспомогательных устройств для пациентов с ограниченными возможностями", "Мешок дренажный раневый")));
            // анестезия
            CDAAnesthetic anesthetic = new CDAAnesthetic(new CDACodifCode("code", "2", "1.2.643.5.1.13.13.11.1033", "Виды анестезии", "Местная"));
            CDAExternalDocReference reference = new CDAExternalDocReference(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754858"));

            return new CDAOperation(code: code,
                                    text: text,
                                    statusCode: statusCode,
                                    time: time,
                                    performers: performers,
                                    lstEquipment: lstEquipment,
                                    manProducts: manProducts,
                                    anesthetic: anesthetic,
                                    reference: reference);
        }
    }
}
