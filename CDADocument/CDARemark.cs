﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Примечание к исследованиям и т.д.
    class CDARemark : ICDASerializable
    {
        private CDACodifCode code;
        private CDAText text;
        private CDAAuthor author;

        public CDARemark(CDACodifCode code, CDAText text, CDAAuthor author)
        {
            this.code = code;
            this.text = text;
            this.author = author;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAActStatement remark = CDAActStatement.CreateAct(code, text, author);
            return remark.ToXmlElement(document);
        }
    }
}
