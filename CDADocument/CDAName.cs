﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // ФИО
    public class CDAName : ICDASerializable
    {
        private CDAString surname;
        private CDAString name1;
        private CDAString name2;
        private nullFlavor nF;

        // Конструктор, задающий ФИО
        public CDAName(CDAString surname, CDAString name1, CDAString name2)
        {
            this.surname = surname;
            this.name1 = name1;
            this.name2 = name2;
            nF = null;
        }

        // Конструктор, задающий nullFlavor
        public CDAName(nullFlavor nF)
        {
            this.nF = nF;
        }

        public string GetFIO()
        {
            if (nF == null)
            {
                string res = "";
                if (surname.Value != null)
                    res = res + surname.Value;
                if (name1.Value != null)
                {
                    if (res != "")
                        res = res + " ";
                    res = res + name1.Value.Substring(0, 1) + ".";
                }
                if (name2.Value != null)
                {
                    if (res != "")
                        res = res + " ";
                    res = res + name1.Value.Substring(0, 1) + ".";
                }
                if (res == "")
                    res = null;
                return res;
            }
            else
                return null;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elName = document.CreateElement("name");

            if (nF != null)
                elName = nF.ToXmlElementAtr(elName);
            else
            {
                // фамилия
                elName.AppendChild(surname.ToXmlElementVal(document.CreateElement("family")));
                // имя
                elName.AppendChild(name1.ToXmlElementVal(document.CreateElement("given")));
                // отчество
                elName.AppendChild(name2.ToXmlElementVal(document.CreateElement("given")));
            }
            return elName;
        }
    }
}
