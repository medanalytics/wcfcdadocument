﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAVisit : ICDASerializable
    {
        private CDATime time;
        private CDAPerformer performer;
        private CDACodifCode location; // место посещения
        private List<CDAService> services; // оказанные услуги
        private CDACodeVal complaint; // жалоба
        private CDACodeVal objective; // объективные данные
        private CDACodeVal conclusion; // заключение
        private CDADiagnosis diagnosis; // диагноз
        private CDACodeVal recomendation; // рекомендации

        public string Date
        { get { return time.High.ToString("dd.MM.yyyy"); } }

        public string Performer
        { get { return performer.GetPerformer(); } }

        public string Complaint
        { get { return complaint.Value.Value; } }

        public string ObjectiveData
        { get { return objective.Value.Value; } }

        public string Conclusion
        { get { return conclusion.Value.Value; } }

        public string Recomendation
        { get { return recomendation.Value.Value; } }

        public CDAVisit(CDATime time, CDAPerformer performer, CDACodifCode location, List<CDAService> services,
            CDACodeVal complaint, CDACodeVal objective, CDACodeVal conclusion, CDADiagnosis diagnosis, CDACodeVal recomendation)
        {
            this.time = time;
            this.performer = performer;
            this.location = location;
            this.services = services;
            this.complaint = complaint;
            this.objective = objective;
            this.conclusion = conclusion;
            this.diagnosis = diagnosis;
            this.recomendation = recomendation;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(time);
            elements.Add(performer);
            elements.Add(new CDAContainer("participant", "LOC", 
                            new CDAContainer("participantRole", 
                                location, 
                                new CDAElementAttribute("classCode", "SDLOC"))));
            foreach (CDAService service in services)
                elements.Add(new CDAContainer("entryRelationship", service,
                                new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (complaint != null)
                elements.Add(new CDAContainer("entryRelationship", complaint,
                                new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (objective != null)
                elements.Add(new CDAContainer("entryRelationship", objective,
                                new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (conclusion != null)
                elements.Add(new CDAContainer("entryRelationship", conclusion,
                                new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (diagnosis != null)
                elements.Add(new CDAContainer("entryRelationship", diagnosis,
                                new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));

            return CDAActStatement.CreateEncounter(elements).ToXmlElement(document);
        }

        public static CDAVisit Create()
        {
            CDATime time = new CDATime(new DateTime(2016, 08, 08, 07, 07, 00, DateTimeKind.Local), new DateTime(2016, 08, 08, 07, 37, 00, DateTimeKind.Local));
            // Исполнитель
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.Create("2341"));
            // Место посещения
            CDACodifCode location = new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1008", "Место оказания медицинской помощи", "Амбулаторно-поликлиническое учреждение");
            // Оказанные услуги
            List<CDAService> services = new List<CDAService>();
            services.Add(new CDAService(new CDACodifCode("code", "B01.057.001", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинсках услуг", "Прием (осмотр, консультация) врача-хирурга первичный"),
                       new CDATime(new DateTime(2016, 08, 08, 07, 07, 00, DateTimeKind.Local))));            
            // Жалобы
            CDACodeVal complaint = new CDACodeVal(
                        new CDACodifCode("code", "835", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Жалобы пациента"),
                        new CDAValue("На сильную боль в правой кисти."));            
            // Объективные данные
            CDACodeVal objective = new CDACodeVal(
                        new CDACodifCode("code", "836", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Объективные данные"),
                        new CDAValue("На II-III пальцах правой кисти имеются ожоговые раны до 2,0 см без отделяемого, с активной краевой и островковой эпителизацией. Признаков перифокального воспаления нет."));            
            // Заключение
            CDACodeVal conclusion = new CDACodeVal(
                        new CDACodifCode("code", "837", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Заключение"),
                        new CDAValue("Ожог запястья и кисти второй степени."));            
            // Заключительный диагноз
            CDADiagnosis diagnosis = CDADiagnosis.CreateFinal();
            // Рекомендации
            CDACodeVal recomendation = new CDACodeVal(
                        new CDACodifCode("code", "839", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Рекомендации"),
                        new CDAValue("Санаторно-курортное лечение."));

            return new CDAVisit(time: time,
                                performer: performer,
                                location: location,
                                services: services,
                                complaint: complaint,
                                objective: objective,
                                conclusion: conclusion,
                                diagnosis: diagnosis,
                                recomendation: recomendation);
        }
    }
}
