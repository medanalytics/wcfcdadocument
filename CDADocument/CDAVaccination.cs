﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAVaccination : ICDASerializable
    {
        private CDAText text;
        private CDATime time;
        private CDAVaccine vaccine;

        public string Date
        { get { return time.GetDate(); } }

        public CDAVaccine Vaccine
        { get { return vaccine; } }

        public string Comment
        { get { return text.Value; } }

        public CDAVaccination(CDAVaccine vaccine, CDATime time, CDAText text)
        {
            this.vaccine = vaccine;
            this.time = time;
            this.text = text;
        }

        public void AddTextReference(CDATextReference reference)
        {
            vaccine.Code.AddComponent(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> Elements = new List<ICDASerializable>();
            if (text != null)
                Elements.Add(text);
            Elements.Add(time);
            Elements.Add(vaccine);
            CDAContainer container = new CDAContainer("substanceAdministration", Elements,
                                        new CDAElementAttribute("classCode", "SBADM"), new CDAElementAttribute("moodCode", "EVN"));
            return container.ToXmlElement(document);
        }

        public static CDAVaccination Create()
        {
            CDAVaccine vaccine = new CDAVaccine(new CDACodifCode("code", "261", "1.2.643.5.1.13.13.11.1078", "Иммунобиологические препараты для специфической профилактики, диагностики и терапии", "Вакцина коклюшно-дифтерийно-столбнячная адсорбированная жидкая с уменьшенным содержанием антигенов (АКДС-М-вакцина)"));
            CDATime time = new CDATime(new DateTime(2016, 10, 02));
            CDAText text = new CDAText("Текстовый комментарий");

            return new CDAVaccination(vaccine, time, text);
        }
    }
}
