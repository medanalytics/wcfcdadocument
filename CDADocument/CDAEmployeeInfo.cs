﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Информация о сотруднике мед. учреждения
    public class CDAEmployeeInfo
    {
        // Уникальный идентификатор в МИС,
        // Идентификатор по Федеральному Регистру Медицинских работников,
        // СНИЛС
        private List<CDARootExt> Ids;
        // Должность
        private CDACodifCode Job;
        // Адрес
        public CDAAddress Address;
        // Контакты
        public List<CDATelecom> Contacts;
        // ФИО
        public CDAName Name;
        // Организация
        public CDAOrganisation Organisation;

        public CDAEmployeeInfo(List<CDARootExt> Ids, CDACodifCode Job, CDAAddress Address, List<CDATelecom> Contacts, CDAName Name, CDAOrganisation Organisation)
        {
            this.Ids = Ids;
            this.Job = Job;
            this.Address = Address;
            this.Contacts = Contacts;
            this.Name = Name;
            this.Organisation = Organisation;
        }

        public string GetFIO()
        {
            return Name.GetFIO();
        }

        public string GetJob()
        {
            return Job.GetDisplayName();
        }

        // Добавить специальность в элемент с должностью
        public CDAEmployeeInfo AddSpeciality()
        {
            Job.AddComponent(new CDACodifCode("translation", "30", "1.2.643.5.1.13.13.11.1066", "Номенклатура специальностей специалистов со средним, высшим и послевузовским медицинским и фармацевтическим образованием в сфере здравоохранения", "Хирургия"));
            return this;
        }


        public List<XmlElement> ToXmlElements(XmlDocument document)
        {
            List<XmlElement> lstElEmpl = new List<XmlElement>();
            // id
            foreach (CDARootExt id in Ids)
                lstElEmpl.Add(id.ToXmlElementAtr(document.CreateElement("id")));
            // Должность
            lstElEmpl.Add(Job.ToXmlElement(document));
            // Адрес
            if (Address != null)
                lstElEmpl.Add(Address.ToXmlElement(document));
            // Контакты
            foreach (CDATelecom contact in Contacts)
                lstElEmpl.Add(contact.ToXmlElement(document));
            // ФИО
            XmlElement elPerson = document.CreateElement("assignedPerson");
            elPerson.AppendChild(Name.ToXmlElement(document));
            lstElEmpl.Add(elPerson);
            // Организация
            XmlElement elOrg = document.CreateElement("representedOrganization");
            foreach (XmlElement element in Organisation.ToXmlElements(document))
                elOrg.AppendChild(element);
            lstElEmpl.Add(elOrg);

            return lstElEmpl;
        }

        public static CDAEmployeeInfo Create(string id)
        {
            if (id == "2341")
            {
                // Идентификаторы сотрудника
                List<CDARootExt> Ids = new List<CDARootExt>();
                Ids.Add(new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.70", id)); // идентификатор в МИС
                Ids.Add(new CDARootExt("1.2.643.100.3", "321-654-777 09")); // СНИЛС
                Ids.Add(new CDARootExt("1.2.643.5.1.13.2.1.1.1504.100", "2235")); // идентификатор в Федеральном Регистре
                // Должность
                CDACodifCode Job = new CDACodifCode("73", "1.2.643.5.1.13.2.1.1.607", "Номенклатура должностей медицинских работников и фармацевтических работников", "врач-терапевт");
                // Контакты
                List<CDATelecom> Contacts = new List<CDATelecom>();
                Contacts.Add(new CDATelecom("tel:+74954241311"));
                Contacts.Add(new CDATelecom("tel:+79261234588", CDATelecom.Kind.MC));
                // ФИО
                CDAName Name = new CDAName(new CDAString("Привалов"), new CDAString("Александр"), new CDAString("Иванович"));
                // Адрес
                CDAAddress Address = new CDAAddress(new CDAString("125040, Ленинградский проспект, дом 78, корпус 3, кв. 12"), new CDAString("77"));
                // Организация
                CDAOrganisation Organisation = CDAOrganisation.CreateMO("1.2.643.5.1.13.3.25.77.923");

                return new CDAEmployeeInfo(Ids: Ids, Job: Job, Address: Address, Contacts: Contacts, Name: Name, Organisation: Organisation);
            }
            else
            {
                // Идентификаторы сотрудника
                List<CDARootExt> Ids = new List<CDARootExt>();
                Ids.Add(new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.70", id)); // идентификатор в МИС
                Ids.Add(new CDARootExt("1.2.643.100.3", "885-996-741 11")); // СНИЛС
                Ids.Add(new CDARootExt("1.2.643.5.1.13.2.1.1.1504.100", "2236")); // идентификатор в Федеральном Регистре
                // Должность
                CDACodifCode Job = new CDACodifCode("10234", "1.2.643.5.1.13.2.1.1.607", "Номенклатура должностей медицинских работников и фармацевтических работников", "заведующий (начальник) структурного подразделения (отдела, отделения, лаборатории, кабинета, отряда и другое) медицинской организации - врач-специалист");
                // Контакты
                List<CDATelecom> Contacts = new List<CDATelecom>();
                Contacts.Add(new CDATelecom("tel:+74954244567"));
                Contacts.Add(new CDATelecom("tel:+79031234588", CDATelecom.Kind.MC));
                Contacts.Add(new CDATelecom("mailto:steaf@gmail.com"));
                Contacts.Add(new CDATelecom("fax:+74954244567"));
                // ФИО
                CDAName Name = new CDAName(new CDAString("Степанов"), new CDAString("Андрей"), new CDAString("Фёдорович"));
                // Адрес
                CDAAddress Address = new CDAAddress(new CDAString("119192, г.Москва, Мичуринский проспект, дом 16, кв. 9."), new CDAString("77"));
                // Организация
                CDAOrganisation Organisation = CDAOrganisation.CreateMO("1.2.643.5.1.13.3.25.77.923");

                return new CDAEmployeeInfo(Ids: Ids, Job: Job, Address: Address, Contacts: Contacts, Name: Name, Organisation: Organisation);
            }
        }

        // Без адреса и контактов
        public static CDAEmployeeInfo CreateShort(string id)
        {
            List<CDARootExt> Ids = new List<CDARootExt>();
            CDACodifCode Job = null;
            List<CDATelecom> Contacts = new List<CDATelecom>();
            CDAName Name = null;
            CDAAddress Address = null;
            CDAOrganisation Organisation = null;

            // Идентификаторы сотрудника                
            Ids.Add(new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.70", id)); // идентификатор в МИС
            Ids.Add(new CDARootExt("1.2.643.100.3", "445-784-445 10")); // СНИЛС
            Ids.Add(new CDARootExt("1.2.643.5.1.13.2.1.1.1504.100", "2237")); // идентификатор в Федеральном Регистре
            // Должность
            Job = new CDACodifCode("62", "1.2.643.5.1.13.2.1.1.607", "Номенклатура должностей медицинских работников и фармацевтических работников", "врач-рентгенолог");
            // ФИО
            Name = new CDAName(new CDAString("Дауров"), new CDAString("Роман"), new CDAString("Борисович"));
            // Организация
            Organisation = CDAOrganisation.CreateMO("1.2.643.5.1.13.3.25.77.923");

            return new CDAEmployeeInfo(Ids: Ids, Job: Job, Address: Address, Contacts: Contacts, Name: Name, Organisation: Organisation);
        }
    }
}
