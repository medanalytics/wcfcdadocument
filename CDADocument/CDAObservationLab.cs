﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Лабораторный анализ
    public class CDAObservationLab : ICDASerializable
    {
        private CDACodifCode code;
        private CDACodifCode statusCode;
        private CDATime time;
        private CDACodifCode priorityCode;
        private CDAValue value;
        private CDAPerformer performer;        
        private CDAExternalDocReference reference;        

        public string Date
        { get { return time.GetDate(); } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Result
        { get { return value.Value; } }

        public string Priority
        { get { return priorityCode.GetDisplayName(); } }

        public CDAObservationLab(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, CDAExternalDocReference reference)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;            
            this.reference = reference;                
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(code);
            elements.Add(statusCode);
            elements.Add(time);
            elements.Add(priorityCode);
            elements.Add(value);
            elements.Add(performer);            
            if (reference != null)
                elements.Add(reference);            
            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }

        public static CDAObservationLab Create()
        {
            CDACodifCode code =  new CDACodifCode("code", "101", "1.2.643.5.1.13.2.1.1.1504.10", "Справочник типов лабораторных исследований", "Общеклинический анализ крови");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2014, 05, 15, 14, 30, 0, DateTimeKind.Local));            
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");
            CDAValue value =  new CDAValue(@"Результаты и\или заключение");
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));            
            // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(
                new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754869"));

            return new CDAObservationLab(code: code,
                                         statusCode: statusCode,
                                         time: time,
                                         priorityCode: priorityCode,
                                         value: value,
                                         performer: performer,
                                         reference: reference);
        }
    }
}
