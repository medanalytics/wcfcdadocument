﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Элемент value
    public class CDAValue : ICDASerializable
    {
        private enum Type
        { CD, ST, PQ, IVL_PQ }

        private Type type;
        private string TextVal;
        private CDACodifCode CodeVal;
        private CDAQuantity rangeLow;
        private CDAQuantity rangeHigh;

        public string Value
        {
            get
            {
                if (type == Type.PQ)
                    return rangeLow.Value;
                else if (type == Type.CD)
                    return CodeVal.GetDisplayName();
                else if (type == Type.ST)
                    return TextVal;
                else
                    return null;
            }
        }

        public string Unit
        {
            get
            {
                if (type == Type.PQ)
                    return rangeLow.Unit;
                else
                    return null;
            }
        }

        public CDAValue(string TextVal)
        {
            this.TextVal = TextVal;
            this.CodeVal = null;
            type = Type.ST;
        }

        public CDAValue(CDACodifCode CodeVal)
        {
            this.TextVal = null;
            this.CodeVal = CodeVal;
            type = Type.CD;
        }

        public CDAValue(CDAQuantity quantity)
        {
            this.rangeLow = quantity;
            type = Type.PQ;
        }

        public CDAValue(CDAQuantity low, CDAQuantity high)
        {
            this.rangeLow = low;
            this.rangeHigh = high;
            type = Type.IVL_PQ;
        }

        public void SetReference(CDATextReference reference)
        {
            if (type == Type.CD)
                CodeVal.SetReference(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elVal = document.CreateElement("value");
            XmlAttribute attr = document.CreateAttribute("type", "http://www.w3.org/2001/XMLSchema-instance");
            attr.Value = type.ToString();
            elVal.Attributes.Append(attr);
            if (type == Type.ST)
                elVal.InnerText = TextVal;
            else if (type == Type.CD)
            {
                elVal = CodeVal.ToXmlElementAtr(elVal);
                if (CodeVal.Reference != null)
                    elVal.AppendChild(CodeVal.Reference.ToXmlElement(document));
            }
            else if (type == Type.PQ)
                elVal = rangeLow.ToXmlElementAtr(elVal);
            else if (type == Type.IVL_PQ)
            {
                elVal.AppendChild(rangeLow.ToXmlElementAtr(document.CreateElement("low")));
                elVal.AppendChild(rangeHigh.ToXmlElementAtr(document.CreateElement("high")));
            }

            return elVal;
        }
    }
}
