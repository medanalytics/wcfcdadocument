﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Информация о лице, придавшем юридическую силу документу
    public class CDASignature : ICDASerializable
    {
        private CDATime SignTime;
        private CDACodifCode SignCode;
        private CDAEmployeeInfo Authenticator;

        public CDASignature(CDAEmployeeInfo Authenticator, CDATime SignTime, CDACodifCode SignCode)
        {
            this.SignTime = SignTime;
            this.SignCode = SignCode;
            this.Authenticator = Authenticator;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elAuthenticator = document.CreateElement("legalAuthenticator");
            // время подписания
            elAuthenticator.AppendChild(SignTime.ToXmlElement(document));
            // наличие подписи
            elAuthenticator.AppendChild(SignCode.ToXmlElementAtr(document.CreateElement("signatureCode")));
            // автор
            XmlElement elAssigned = document.CreateElement("assignedEntity");
            foreach (XmlElement element in Authenticator.ToXmlElements(document))
                elAssigned.AppendChild(element);

            elAuthenticator.AppendChild(elAssigned);

            return elAuthenticator;
        }

        public static CDASignature Create()
        {
            CDATime SignTime = new CDATime(new DateTime(2017, 08, 31, 16, 00, 00, DateTimeKind.Local), "time");
            CDACodifCode SignCode = new CDACodifCode("S", null, null, null);
            CDAEmployeeInfo Authenticator = CDAEmployeeInfo.Create("2342");

            return new CDASignature(Authenticator, SignTime, SignCode);
        }
    }
}
