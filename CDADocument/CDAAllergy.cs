﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Аллергия
    class CDAAllergy : ICDASerializable
    {
        CDACodifCode code; // тип реакции
        CDAText comment; // комментарии
        CDATime date; // дата выявления
        CDAAllergen allergen; // аллерген
        CDAAlergyReaction reaction; // реакция

        public CDAAllergen Allergen
        { get { return allergen; } }

        public CDAAlergyReaction Reaction
        { get { return reaction; } }

        public string Date
        { get { return date.ToString(); } }

        public string Comment
        { get { return comment.Value; } }

        public string Type
        { get { return code.GetDisplayName(); } }

        public CDAAllergy(CDACodifCode code, CDAText comment, CDATime date, CDAAllergen allergen, CDAAlergyReaction reaction)
        {
            this.code = code;
            this.comment = comment;
            this.date = date;
            this.allergen = allergen;
            this.reaction = reaction;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAActStatement allergy = CDAActStatement.CreateObservation(code, comment, date, allergen, reaction);
            return allergy.ToXmlElement(document);
        }

        public static CDAAllergy Create(string id)
        {
            if (id == "")
            {
                CDACodifCode code = new CDACodifCode("code", "21", "1.2.643.5.1.13.13.11.1064", "Тип патологической реакции для сбора аллергоанамнеза", "Лекарственная аллергия");
                CDAText comment = new CDAText("-");
                CDATime date = new CDATime(new DateTime(2016, 08, 08));
                CDAAllergen allergen = new CDAAllergen(new CDACodifCode("code", "137", "1.2.643.5.1.13.2.1.1.179", "Классификатор международных непатентованных наименований лекарственных средств", "Ампициллин"));
                CDAAlergyReaction reaction = new CDAAlergyReaction(new CDACodifCode("code", "7", "1.2.643.5.1.13.13.11.1063", "Основные клинические проявления патологических реакций", "Крапивница"));

                return new CDAAllergy(code, comment, date, allergen, reaction);
            }
            else
            {
                CDACodifCode code = new CDACodifCode("code", "24", "1.2.643.5.1.13.13.11.1064", "Тип патологической реакции для сбора аллергоанамнеза", "Другие аллергии");
                CDAText comment = new CDAText("=^..^=");
                CDATime date = new CDATime(new DateTime(2016, 09, 09));
                CDAAllergen allergen = new CDAAllergen("Котики");
                CDAAlergyReaction reaction = new CDAAlergyReaction(new CDACodifCode("code", "10", "1.2.643.5.1.13.13.11.1063", "Основные клинические проявления патологических реакций", "Ринит"));

                return new CDAAllergy(code, comment, date, allergen, reaction);
            }
        }
    }
}
