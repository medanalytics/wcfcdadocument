﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Список в текстовом наполнении секции
    public class CDATextList
    {
        private XmlElement elList;
        private XmlDocument document;
        private string Caption;

        public CDATextList(XmlElement elList, XmlDocument document, string Caption, params CDAElementAttribute[] Attributes)
        {
            this.elList = elList;
            this.document = document;
            // применим атрибуты к элементу <list>
            foreach (CDAElementAttribute attribute in Attributes)
                elList = attribute.ToXmlElementAtr(elList);
            // зададим <caption>
            if (Caption != null)
            {
                XmlElement elCaption = document.CreateElement("caption");
                elCaption.InnerXml = Caption;
                elList.AppendChild(elCaption);
            }
        }

        public void AddItem(string Item)
        {
            XmlElement elItem = document.CreateElement("item");
            elItem.InnerXml = Item;
            elList.AppendChild(elItem);
        }
    }
}
