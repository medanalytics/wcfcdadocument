﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDADocumentSection : ICDASerializable
    {
        private List<ICDASerializable> elements;

        public CDADocumentSection(List<ICDASerializable> elements)
        {
            this.elements = elements;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elSection = document.CreateElement("section");
            foreach (ICDASerializable element in elements)
                elSection.AppendChild(element.ToXmlElement(document));
            return elSection;
        }
    }
}
