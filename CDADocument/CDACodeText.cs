﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDACodeText : ICDASerializable
    {
        private CDACodifCode code;
        private CDAText text;

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Text
        { get { return text.Value; } }

        public CDACodeText(CDACodifCode code, CDAText text)
        {
            this.code = code;
            this.text = text;
        }

        public void SetReference(CDATextReference reference)
        {
            code.SetReference(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            return CDAActStatement.CreateObservation(code, text).ToXmlElement(document);
        }
    }
}
