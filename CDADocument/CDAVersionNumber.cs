﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAVersionNumber : ICDASerializable
    {
        private string Value;

        public CDAVersionNumber(string Value)
        {
            this.Value = Value;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elVersion = document.CreateElement("versionNumber");
            elVersion.SetAttribute("value", Value);
            return elVersion;
        }
    }
}
