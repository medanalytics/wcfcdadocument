﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Класс, создающий элементы act, encounter, observation, procedure
    public class CDAActStatement : ICDASerializable
    {
        private string Type;
        private string ClassCode;
        private string MoodCode;
        private List<ICDASerializable> Elements;

        public CDAActStatement(string Type, string ClassCode, string MoodCode, List<ICDASerializable> Elements)
        {
            this.Type = Type;
            this.ClassCode = ClassCode;
            this.MoodCode = MoodCode;
            this.Elements = Elements;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elStatement = document.CreateElement(Type);
            elStatement.SetAttribute("classCode", ClassCode);
            elStatement.SetAttribute("moodCode", MoodCode);
            foreach (ICDASerializable element in Elements)
                elStatement.AppendChild(element.ToXmlElement(document));

            return elStatement;
        }

        public static CDAActStatement CreateObservation(params ICDASerializable[] Elements)
        {
            string Type = "observation";
            string ClassCode = "OBS";
            string MoodCode = "EVN";
            List<ICDASerializable> ElList = new List<ICDASerializable>();
            foreach (ICDASerializable element in Elements)
                ElList.Add(element);

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }

        public static CDAActStatement CreateObservation(List<ICDASerializable> ElList)
        {
            string Type = "observation";
            string ClassCode = "OBS";
            string MoodCode = "EVN";

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }

        public static CDAActStatement CreateProcedure(List<ICDASerializable> ElList)
        {
            string Type = "procedure";
            string ClassCode = "PROC";
            string MoodCode = "EVN";

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }

        public static CDAActStatement CreateAct(params ICDASerializable[] Elements)
        {
            string Type = "act";
            string ClassCode = "ACT";
            string MoodCode = "EVN";
            List<ICDASerializable> ElList = new List<ICDASerializable>();
            foreach (ICDASerializable element in Elements)
                ElList.Add(element);

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }

        public static CDAActStatement CreateAct(List<ICDASerializable> ElList)
        {
            string Type = "act";
            string ClassCode = "ACT";
            string MoodCode = "EVN";

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }

        public static CDAActStatement CreateEncounter(List<ICDASerializable> ElList)
        {
            string Type = "encounter";
            string ClassCode = "ENC";
            string MoodCode = "EVN";

            return new CDAActStatement(Type, ClassCode, MoodCode, ElList);
        }
    }
}
