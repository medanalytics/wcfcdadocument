﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Образец лабораторного исследования
    class CDASpecimen : ICDASerializable
    {
        CDARootExt id;
        CDACodifCode code;
        CDAQuantity quantity;
        string desc;

        public CDASpecimen(CDARootExt id, CDACodifCode code, CDAQuantity quantity, string desc)
        {
            this.id = id;
            this.code = code;
            this.quantity = quantity;
            this.desc = desc;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elSpecimen = document.CreateElement("specimen");
            XmlElement elRole = document.CreateElement("specimenRole");
            elRole.AppendChild(id.ToXmlElement(document));
            XmlElement elEntity = document.CreateElement("specimenPlayingEntity");
            elEntity.AppendChild(code.ToXmlElement(document));
            elEntity.AppendChild(quantity.ToXmlElement(document));
            XmlElement elDesc = document.CreateElement("desc");
            elDesc.InnerXml = desc;
            elEntity.AppendChild(elDesc);
            elRole.AppendChild(elEntity);
            elSpecimen.AppendChild(elRole);

            return elSpecimen;
        }

        public static CDASpecimen Create(string id)
        {
            return new CDASpecimen(
                            new CDARootExt("id", "1.2.643.5.1.13.3.25.77.231.100.1.1.66", "124562156"),
                            new CDACodifCode("code", "108", "1.2.643.5.1.13.13.11.1081", "ФСЛИ. Справочник материалов для проведения лабораторного исследования", "Венозная кровь"),
                            new CDAQuantity("6.5", "мл"), "Венозная кровь");
        }
    }
}
