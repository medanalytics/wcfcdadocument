﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Лабораторное исследование
    class CDALabObservation : ICDASerializable
    {
        private CDACodifCode statusCode;
        private CDARootExt reference; // ссылка на исходный документ-исследование
        private List<CDALabFactorGroup> factorGroups; // список групп лабораторных показателей
        private CDARemark remark;

        public List<CDALabFactorGroup> LabFactorGroups
        { get { return factorGroups; } }

        public CDALabObservation(CDACodifCode statusCode, CDARootExt reference, List<CDALabFactorGroup> factorGroups, CDARemark remark)
        {
            this.statusCode = statusCode;
            this.reference = reference;
            this.factorGroups = factorGroups;
            this.remark = remark;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elOrganizer = document.CreateElement("organizer");
            elOrganizer.SetAttribute("classCode", "CLUSTER");
            elOrganizer.SetAttribute("moodCode", "EVN");
            elOrganizer.AppendChild(statusCode.ToXmlElement(document));
            if (reference != null)
            {
                CDAContainer contReference = new CDAContainer("reference", new CDAContainer("externalDocument", reference), new CDAElementAttribute("typeCode", "REFR"));
                elOrganizer.AppendChild(contReference.ToXmlElement(document));
            }
            foreach (CDALabFactorGroup factorGroup in factorGroups)
            {
                XmlElement elComponent = document.CreateElement("component");
                elComponent.AppendChild(factorGroup.ToXmlElement(document));
                elOrganizer.AppendChild(elComponent);
            }
            if (remark != null)
            {
                CDAContainer contRemark = new CDAContainer("component", remark);
                elOrganizer.AppendChild(contRemark.ToXmlElement(document));
            }

            return elOrganizer;
        }

        public static CDALabObservation Create(string id)
        {
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDARootExt reference = new CDARootExt("id", "1.2.643.5.1.13.3.25.77.231.100.1.1.51", "8754869");
            List<CDALabFactorGroup> labFactorGroups = new List<CDALabFactorGroup>();
            labFactorGroups.Add(CDALabFactorGroup.Create("id исследования"));
            CDARemark remark = new CDARemark(
                                    new CDACodifCode("code", "901", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Текстовое заключение по проведенным лабораторным исследованиям"),
                                    new CDAText("Идиопатическая эритроцитимия. Кровянная химера не выявлена."),
                                    new CDAAuthor(CDAEmployeeInfo.CreateShort("2341"), new CDATime(new DateTime(2016, 09, 02, 11, 11, 00, DateTimeKind.Local), "time")));

            return new CDALabObservation(statusCode, reference, labFactorGroups, remark);
        }
    }
}
