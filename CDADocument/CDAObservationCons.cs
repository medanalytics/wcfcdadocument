﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Консультации врача
    public class CDAObservationCons : ICDASerializable
    {
        private CDACodifCode code;
        private CDACodifCode statusCode;
        private CDATime time;
        private CDACodifCode priorityCode;
        private CDAValue value;
        private CDAPerformer performer;        
        private CDAExternalDocReference reference;        

        public string Date
        { get { return time.GetDate(); } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Result
        { get { return value.Value; } }

        public string Priority
        { get { return priorityCode.GetDisplayName(); } }

        public CDAObservationCons(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, CDAExternalDocReference reference)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;            
            this.reference = reference;                        
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(code);
            elements.Add(statusCode);
            elements.Add(time);
            elements.Add(priorityCode);
            elements.Add(value);
            elements.Add(performer);            
            if (reference != null)
                elements.Add(reference);
            
            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }

        public static CDAObservationCons CreateStac()
        {
            CDACodifCode code =  new CDACodifCode("code", "11", "1.2.643.5.1.13.2.1.1.1504.12", "Справочник типов консультаций", "Консультация кардиолога");
            CDACodifCode statusCode =  new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2014, 05, 15, 12, 40, 0, DateTimeKind.Local));                
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");                
            CDAValue value = new CDAValue(@"ИБС, стенокардия напряжения, 3ФК. Атеросклеротический кардиосклероз, постоянная форма мерцательной аритмии, ХСН 2А. 3ФК. Гипертоническая болезнь 2 стадии, АГ 2 степени, риск 4.Ожирение 1 степени");
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));            
                // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(
                    new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754860"));

            return new CDAObservationCons(code: code,
                                         statusCode: statusCode,
                                         time: time,
                                         priorityCode: priorityCode,
                                         value: value,
                                         performer: performer,
                                         reference: reference);
        }
    }
}
