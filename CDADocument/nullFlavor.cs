﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // тип nullFlavor
    public enum nFValue
    { NI, NA, UNK, ASKU, NAV, NASK, MSK }
    
    public class nullFlavor
    {
        private nFValue value; // причина отсутствия значения

        public nullFlavor(nFValue value)
        {
            this.value = value;
        }

        public XmlElement ToXmlElementAtr(XmlElement element)
        {
            element.SetAttribute("nullFlavor", value.ToString());
            return element;
        }
    }
}
