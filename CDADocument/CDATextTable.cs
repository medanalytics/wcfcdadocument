﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Таблица в текстовой части секции
    public class CDATextTable
    {
        private XmlElement elTable;
        private XmlElement elBody;
        private XmlDocument document;
        private string Caption;
        private int numCol;

        public CDATextTable(XmlElement elTable, XmlDocument document, string Caption, params int[] ColWidth)
        {
            this.elTable = elTable;
            this.document = document;
            this.Caption = Caption;
            numCol = ColWidth.Length;
            // Добавим заголовок таблицы
            if (Caption != null)
            {
                XmlElement elCaption = document.CreateElement("caption");
                elCaption.InnerXml = Caption;
                elTable.AppendChild(elCaption);
            }
            // Зададим ширину колонок
            foreach (int width in ColWidth)
            {
                XmlElement elWidth = document.CreateElement("col");
                elWidth.SetAttribute("width", width.ToString() + "%");
                elTable.AppendChild(elWidth);
            }
            elBody = document.CreateElement("tbody");
            elTable.AppendChild(elBody);
        }

        public CDATextTable(XmlElement elTable, XmlDocument document, int numCol)
        {
            this.elTable = elTable;
            this.document = document;
            this.Caption = null;
            this.numCol = numCol;
            elBody = document.CreateElement("tbody");
            elTable.AppendChild(elBody);
        }

        public CDATextTable(XmlElement elTable, XmlDocument document, params string[] Columns)
        {
            this.elTable = elTable;
            this.document = document;
            this.Caption = null;
            this.numCol = Columns.Length;
            elBody = document.CreateElement("tbody");
            elTable.AppendChild(elBody);
            AddHeader(Columns);
        }

        public void AddHeader(params string[] Columns)
        {
            XmlElement elRow = document.CreateElement("tr");
            foreach (string column in Columns)
            {
                XmlElement elCol = document.CreateElement("th");
                elCol.InnerXml = column;
                elRow.AppendChild(elCol);
            }
            elBody.AppendChild(elRow);
        }

        public void AddRow(params string[] Columns)
        {
            XmlElement elRow = document.CreateElement("tr");
            foreach (string column in Columns)
            {
                XmlElement elCol = document.CreateElement("td");
                elCol.InnerXml = column;
                elRow.AppendChild(elCol);
            }
            elBody.AppendChild(elRow);
        }

        public void AddSingleRow(string Text, params CDAElementAttribute[] Attributes)
        {
            XmlElement elRow = document.CreateElement("td");
            elRow.SetAttribute("colspan", numCol.ToString());
            XmlElement elContent = document.CreateElement("content");
            foreach (CDAElementAttribute attribute in Attributes)
                elContent = attribute.ToXmlElementAtr(elContent);
            elContent.InnerXml = Text;
            elRow.AppendChild(elContent);
            XmlElement elTr = document.CreateElement("tr");
            elTr.AppendChild(elRow);
            elBody.AppendChild(elTr);
        }
    }
}
