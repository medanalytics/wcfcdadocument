﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Исполнитель мед. услуги
    public class CDAPerformer : ICDASerializable
    {
        private string TypeCode;
        private CDAEmployeeInfo Performer;

        public CDAPerformer(CDAEmployeeInfo Performer)
        {
            this.Performer = Performer;
            TypeCode = null;
        }

        public CDAPerformer(CDAEmployeeInfo Performer, string TypeCode)
        {
            this.Performer = Performer;
            this.TypeCode = TypeCode;
        }

        public string GetPerformer()
        {
            return Performer.GetJob() + "<br/>" + Performer.GetFIO();
        }

        public string GetJob()
        {
            return Performer.GetJob();
        }

        public string GetName()
        {
            return Performer.GetFIO();
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elPerformer = document.CreateElement("performer");
            if (TypeCode != null)
                elPerformer.SetAttribute("typeCode", TypeCode);
            XmlElement elEntity = document.CreateElement("assignedEntity");
            foreach (XmlElement element in Performer.ToXmlElements(document))
                elEntity.AppendChild(element);
            elPerformer.AppendChild(elEntity);

            return elPerformer;
        }
    }
}
