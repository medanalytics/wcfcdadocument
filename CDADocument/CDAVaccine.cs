﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Вакцина
    public class CDAVaccine : ICDASerializable
    {
        private CDACodifCode code;

        public CDACodifCode Code
        { get { return code; } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public CDAVaccine(CDACodifCode code)
        {
            this.code = code;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("consumable", "CSM",
                                        new CDAContainer("manufacturedProduct",
                                            new CDAContainer("manufacturedMaterial",
                                                code,
                                                new CDAElementAttribute("classCode", "MMAT"),
                                            new CDAElementAttribute("determinerCode", "KIND")),
                                            new CDAElementAttribute("classCode", "MANU")));
            return container.ToXmlElement(document);
        }
    }
}
