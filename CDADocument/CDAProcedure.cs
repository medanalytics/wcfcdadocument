﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Медицинская процедура
    public class CDAProcedure : ICDASerializable
    {
        private CDACodifCode code;
        private CDAText text;
        private CDACodifCode statusCode;
        private CDATime time;
        private List<CDAPerformer> performers;
        private List<CDAManufacturedProduct> devices;
        private CDAAnesthetic anesthetic;

        public string Date
        { get { return time.GetDate(); } }

        public string Code
        { get { return code.GetCode(); } }

        public string Name
        { get { return code.GetDisplayName(); } }

        public string Type
        { get { return anesthetic.Code.GetDisplayName(); } }

        public string Performer
        {
            get
            {
                string val = "";
                foreach (CDAPerformer performer in performers)
                {
                    if (val != "")
                        val = val + "<br/><br/>";
                    val = val + performer.GetPerformer();
                }
                return val;
            }
        }

        public string Device
        {
            get
            {
                string val = "";
                foreach (CDAManufacturedProduct device in devices)
                {
                    if (val != "")
                        val = val + ", ";
                    val = val + device.Code.GetDisplayName();
                }
                return val;
            }
        }

        public CDAProcedure(CDACodifCode code, CDAText text, CDACodifCode statusCode, CDATime time,
                            List<CDAPerformer> performers, List<CDAManufacturedProduct> devices, CDAAnesthetic anesthetic)
        {
            this.code = code;
            this.text = text;
            this.statusCode = statusCode;
            this.time = time;
            this.performers = performers;
            this.devices = devices;
            this.anesthetic = anesthetic;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            if (code != null)
                elements.Add(code);
            elements.Add(text);
            elements.Add(statusCode);
            if (time != null)
                elements.Add(time);
            foreach (CDAPerformer performer in performers)
                elements.Add(performer);
            foreach (CDAManufacturedProduct device in devices)
                elements.Add(device);
            if (anesthetic != null)
                elements.Add(anesthetic);
            CDAActStatement procedure = CDAActStatement.CreateProcedure(elements);

            return procedure.ToXmlElement(document);
        }

        public static CDAProcedure Create()
        {
            CDACodifCode code = new CDACodifCode("code", "A11.01.012", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Введение искусственных имплантатов в мягкие ткани");
            CDAText text = new CDAText("Введение искусственных имплантатов в мягкие ткани");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2016, 10, 02, 14, 10, 00, DateTimeKind.Local));
            List<CDAPerformer> performers = new List<CDAPerformer>();
            performers.Add(new CDAPerformer(CDAEmployeeInfo.CreateShort("12121")));
            List<CDAManufacturedProduct> devices = new List<CDAManufacturedProduct>();
            devices.Add(new CDAManufacturedProduct(new CDACodifCode("code", "28", "1.2.643.5.1.13.13.11.1079", "Виды имплантируемых медицинских изделий и вспомогательных устройств для пациентов с ограниченными возможностями", "Мешок дренажный раневый")));
            CDAAnesthetic anesthetic = new CDAAnesthetic(new CDACodifCode("code", "2", "1.2.643.5.1.13.13.11.1033", "Виды анестезии", "Местная"));

            return new CDAProcedure(code, text, statusCode, time, performers, devices, anesthetic);
        }
    }
}
