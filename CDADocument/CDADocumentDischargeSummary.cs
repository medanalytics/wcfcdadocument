﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDADocumentClasses
{
    public static class CDADocumentDischargeSummary
    {
        public static CDADocument Create()
        {
            // Данные для заголовка xml-документа
            string urn = "urn:hl7-org:v3";
            string schema = "http://tech-iemc-test.rosminzdrav.ru/tech/download/infrastructure/1/CDA.xsd";
            string stylesheet = "http://tech-iemc-test.rosminzdrav.ru/tech/download/XSLT/1/DischSum.xsl";

            // Значения, одинаковые для всех отчетов
            CDACodifCode RealmCode = new CDACodifCode("realmCode", "RU", null, null, null);
            CDARootExt typeId = new CDARootExt("typeId", "2.16.840.1.113883.1.3", "POCD_HD000040");
            CDACodifCode ConfidCode = new CDACodifCode("confidentialityCode", "N", "1.2.643.5.1.13.2.1.1.1504.9", "Уровень конфиденциальности документа", "Обычный");
            CDACodifCode LangCode = new CDACodifCode("languageCode", "ru-RU", null, null, null);
            CDARecipient Recipient = new CDARecipient(new CDAOrganisation(new CDARootExt("1.2.643.5.1.13", null), new CDAString("Министерство здравоохранения Российской Федерации (ИЭМК)"), null, new List<CDATelecom>()));
            // Список элементов документа
            List<ICDASerializable> elements = new List<ICDASerializable>();
            
            // Остальные значения берутся из БД
            elements.Add(RealmCode);
            elements.Add(typeId);
            elements.Add(new CDARootExt("templateId", "1.2.643.5.1.13.2.7.5.1.1.3", null));
            // Идентификатор документа
            elements.Add(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "7854321"));
            // Тип документа
            elements.Add(new CDACodifCode("code", "1", "1.2.643.5.1.13.2.1.1.646", "Система электронных медицинских документов", "Эпикриз в стационаре выписной (Эпикриз стационара)"));
            // Заголовок документа
            elements.Add(new CDATitle("Выписной эпикриз"));
            // Время создания документа
            elements.Add(new CDATime(new DateTime(2017, 08, 31, 16, 00, 00, DateTimeKind.Local)));
            // Уровень конфиденциальности
            elements.Add(ConfidCode);
            // Язык
            elements.Add(LangCode);
            // Идентификатор набора версий
            elements.Add(new CDARootExt("setId", "1.2.643.5.1.13.3.25.77.923.100.1.1.50", "7854321"));
            // Номер версии
            elements.Add(new CDAVersionNumber("1"));
            // Пациент
            elements.Add(CDAPatient.Create("585233"));
            // Автор документа
            elements.Add(CDAAuthor.Create());
            // Организация-владелец
            elements.Add(new CDACustodian(CDAOrganisation.CreateCustodian("1.2.643.5.1.13.3.25.77.923")));
            // Получатель
            elements.Add(Recipient);
            // Лицо, придавшее юридическую силу
            elements.Add(CDASignature.Create());
            // Полис ОМС
            elements.Add(CDAPolis.Create());
            // Сведения о случае оказания медицинской помощи
            elements.Add(CDAEncompassingEncounter.Create());
            // Добавим тело документа
            elements.Add(new CDAContainer("component", CreateBody()));
            return new CDADocument(urn, schema, stylesheet, elements);
        }

        public static CDADocumentBody CreateBody()
        {
            List<CDADocumentSection> sections = new List<CDADocumentSection>();
            sections.Add(CreateSectionHOSP());
            sections.Add(CreateSectionSTATEADM());
            sections.Add(CreateSectionSTATEDIS());
            sections.Add(CreateSectionALL());
            sections.Add(CreateSectionSCORES());
            sections.Add(CreateSectionPROC());
            sections.Add(CreateSectionSUM());
            sections.Add(CreateSectionSUR());
            sections.Add(CreateSectionREGIME());
            return new CDADocumentBody(sections);
        }

        public static CDADocumentSection CreateSectionHOSP()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            // Код и название секции
            elements.Add(new CDACodifCode("code", "HOSP", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Пребывание в стационаре"));
            elements.Add(new CDATitle("Общие данные о госпитализации"));
            // Создадим текстовую часть
            CDAText text = new CDAText();
            elements.Add(text);
            // Вид госпитализации
            CDACodeVal hospType = new CDACodeVal(new CDACodifCode("code", "3", "1.2.643.5.1.13.2.1.1.1504", null, "Срочность госпитализации"),
                                                 new CDAValue(new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.1504.3", "Срочность госпитализации", "Плановая госпитализация")));
            elements.Add(new CDAContainer("entry", hospType));
            // Результат стационарного лечения
            CDACodeVal hospResult = new CDACodeVal(new CDACodifCode("code", "688", "1.2.643.5.1.13.2.1.1", null, "Результат стационарного лечения"),
                                                   new CDAValue(new CDACodifCode("3", "1.2.643.5.1.13.2.1.1.688", "Классификатор результатов госпитализации (исхода заболевания, причин выписки). Версия 2", "без перемен")));
            elements.Add(new CDAContainer("entry", hospResult));
            // Добавим в текстовую часть таблицу с общей информацией
            CDATextTable tblInfo = text.AddTable(2);
            tblInfo.AddRow("Вид госпитализации", hospType.Value.Value);
            tblInfo.AddRow("Результат стационарного лечения", hospResult.Value.Value);
            tblInfo.AddRow("Отделение поступления", "3 хирургическое");
            tblInfo.AddRow("Отделение выписки", "3 хирургическое");
            tblInfo.AddRow("Показания (повод) к госпитализации", "Постоянные боли в правом коленном суставе, резкие боли при сгибании в суставе.");
            // Список диагнозов
            List<CDADiagnosisInfo> diags = new List<CDADiagnosisInfo>();
            diags.Add(new CDADiagnosisInfo(new CDACodifCode("code", "1", "1.2.643.5.1.13.2.1.1.1504.4", "Справочник частей диагноза", "Основное заболевание"),
                                           new CDACodifCode("C49.2", "1.2.643.5.1.13.2.1.1.641", "Международная классификация болезней и состояний, связанных со здоровьем, Десятого пересмотра. Версия 2", "Соединительной и мягких тканей нижней конечности, включая тазобедренную область")));
            diags.Add(new CDADiagnosisInfo(new CDACodifCode("code", "3", "1.2.643.5.1.13.2.1.1.1504.4", "Справочник частей диагноза", "Сопутствующая патология"),
                                           new CDACodifCode("I25.1", "1.2.643.5.1.13.2.1.1.641", "Международная классификация болезней и состояний, связанных со здоровьем, Десятого пересмотра. Версия 2", "Атеросклеротическая болезнь сердца")));
            // Создадим таблицу с диагнозами и заполним ее
            text.AddBr(); // Добавим в текст перевод строки
            CDATextTable tblDiag = text.AddTable("Шифр", "Тип", "Текст");
            foreach (CDADiagnosisInfo diag in diags)
            {
                tblDiag.AddRow(diag.Code, diag.Type, diag.Name);
                // добавим диагноз в секцию
                elements.Add(new CDAContainer("entry", diag));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSTATEADM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            // Код и название секции
            elements.Add(new CDACodifCode("code", "STATEADM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Состояние при поступлении"));
            elements.Add(new CDATitle("Состояние при поступлении"));
            // Текстовая часть
            elements.Add(new CDAText("Удовлетворительное. Жалобы при поступлении: на боли в правом коленном суставе. Положение: активное. Местный статус: при осмотре отмечается увеличение в объеме правого коленного сустава при пальпации по внутренней поверхности определяется резкая болезненность пальпаторно уплотнение без четких границ до 6 см в диаметре."));
            // Код состояния при поступлении
            elements.Add(new CDAContainer("entry", new CDACodeVal(new CDACodifCode("code", "111", "1.2.643.5.1.13.2.1.1", null, "Классификатор состояний при обращении (поступлении) в медицинскую организацию"),
                                                new CDAValue(new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.111", "Классификатор состояний при обращении (поступлении) в медицинскую организацию", "Удовлетворительное")))));
            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSTATEDIS()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "STATEDIS", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Состояние при выписке"));
            elements.Add(new CDATitle("Состояние при выписке"));
            elements.Add(new CDAText("Удовлетворительное. Пациентка поступила для дообследования по поводу опухоли с поражением проксимального отдела правой голени и правого коленного сустава. При обследовании по данным морфологии выявлена саркома мягких тканей правой голени низкой степени злокачественности. Учитывая местную распространенность опухолевого процесса пациентке показано хирургическое лечение в объеме удаления опухоли с резекцией проксимального отдела правой большеберцовой кости и эндопротезированием."));
            elements.Add(new CDAContainer("entry", new CDACodeVal(new CDACodifCode("code", "111", "1.2.643.5.1.13.2.1.1", null, "Классификатор состояний при обращении (поступлении) в медицинскую организацию"),
                                                new CDAValue(new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.111", "Классификатор состояний при обращении (поступлении) в медицинскую организацию", "Удовлетворительное")))));
            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionALL()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "ALL", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Аллергии и непереносимость"));
            elements.Add(new CDATitle("Аллергии и непереносимость"));
            elements.Add(new CDAText("16.05.2014, Острая токсико-аллергическая реакция на прием препаратов из группы пенициллина."));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSCORES()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SCORES", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Объективизированная оценка состояния больного"));
            elements.Add(new CDATitle("Объективизированная оценка состояния больного"));
            elements.Add(new CDAText("Пациент скорее жив, чем мертв"));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionPROC()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "PROC", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Исследования и процедуры"));
            elements.Add(new CDATitle("Диагностические исследования и консультации"));
            // Помещаем секции в элемент <component>
            elements.Add(new CDAContainer("component", CreateSectionRESINSTR()));
            elements.Add(new CDAContainer("component", CreateSectionRESLAB()));
            elements.Add(new CDAContainer("component", CreateSectionRESMOR()));
            elements.Add(new CDAContainer("component", CreateSectionRESCONS()));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESINSTR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESINSTR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты инструментальных исследований"));
            elements.Add(new CDATitle("Результаты инструментальных исследований"));
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            // Исследования
            List<CDAObservationInstr> lstObs = new List<CDAObservationInstr>();
            lstObs.Add(CDAObservationInstr.CreateStac());
            // Добавим в текстовый документ таблицу с исследованиями
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Приоритет");
            // Пройдемся по исследованиям и добавим инфу в текстовое наполнение
            foreach (CDAObservationInstr observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Priority);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESLAB()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESLAB", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты инструментальных исследований"));
            elements.Add(new CDATitle("Результаты лабораторных исследований"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Анализы
            List<CDAObservationLab> lstObs = new List<CDAObservationLab>();
            lstObs.Add(CDAObservationLab.Create());
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Приоритет");
            foreach (CDAObservationLab observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Priority);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESMOR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESMOR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты инструментальных исследований"));
            elements.Add(new CDATitle("Результаты морфологических исследований"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Исследования
            List<CDAObservationMor> lstObs = new List<CDAObservationMor>();
            lstObs.Add(CDAObservationMor.CreateStac());
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Приоритет");
            foreach (CDAObservationMor observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Priority);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESCONS()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESCONS", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты инструментальных исследований"));
            elements.Add(new CDATitle("Консультации врачей специалистов"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Исследования
            List<CDAObservationCons> lstObs = new List<CDAObservationCons>();
            lstObs.Add(CDAObservationCons.CreateStac());
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Приоритет");
            foreach (CDAObservationCons observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Priority);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSUM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SUM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Информация о лечении"));
            elements.Add(new CDATitle("Проведенное лечение"));
            elements.Add(new CDAText());

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSUR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SUR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Операции"));
            elements.Add(new CDATitle("Хирургические операции"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Список операций
            List<CDAOperation> operations = new List<CDAOperation>();
            operations.Add(CDAOperation.CreateStac());
            // Заполним текстовую часть и добавим операции в секцию
            CDATextTable table = text.AddTable("Дата", "Операция", "Послеоперационные осложнения");
            foreach (CDAOperation operation in operations)
            {
                table.AddRow(operation.Date, operation.Name, "без осложнений"); // возможно, инфу об осложнениях можно тоже хранить в CDAOperation
                elements.Add(new CDAContainer("entry", operation));
            }
            
            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionREGIME()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "REGIME", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Режим и рекомендации"));
            elements.Add(new CDATitle("Рекомендации"));
            elements.Add(new CDAContainer("component", CreateSectionRECDIET()));
            elements.Add(new CDAContainer("component", CreateSectionRECTREAT()));
            elements.Add(new CDAContainer("component", CreateSectionRECWORK()));
            elements.Add(new CDAContainer("component", CreateSectionRECOTHER()));
            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRECDIET()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RECDIET", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Режим и диета"));
            elements.Add(new CDATitle("Режим и диета"));
            elements.Add(new CDAText("Постельный режим, Щадящая диета (ЩД)."));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRECTREAT()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RECREAT", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Рекомендованное лечение"));
            elements.Add(new CDATitle("Рекомендованное лечение"));
            elements.Add(new CDAText(@"1) Наблюдение районного онколога.<br/>2) Учитывая данные морфологии показано хирургическое лечение."));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRECWORK()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RECWORK", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Трудовые рекомендации"));
            elements.Add(new CDATitle("Трудовые рекомендации"));
            elements.Add(new CDAText("К труду с 01.07.2014."));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRECOTHER()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RECOTHER", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Прочие рекомендации"));
            elements.Add(new CDATitle("Прочие рекомендации"));
            elements.Add(new CDAText("Отсутствуют."));

            return new CDADocumentSection(elements);
        }
    }
}
