﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Лабораторное оборудование
    class CDADevice : ICDASerializable
    {
        private CDARootExt id;
        private string name;

        public string Name
        { get { return name; } }

        public CDADevice(CDARootExt id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elParticipant = document.CreateElement("participant");
            elParticipant.SetAttribute("typeCode", "DEV");
            XmlElement elRole = document.CreateElement("participantRole");
            elRole.SetAttribute("classCode", "ROL");
            elRole.AppendChild(id.ToXmlElement(document));
            XmlElement elDevice = document.CreateElement("playingDevice");
            XmlElement elName = document.CreateElement("manufacturerModelName");
            elName.InnerXml = name;
            elDevice.AppendChild(elName);
            elRole.AppendChild(elDevice);
            elParticipant.AppendChild(elRole);

            return elParticipant;
        }
    }
}
