﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Группа лабораторных показателей
    class CDALabFactorGroup : ICDASerializable
    {
        private CDAText text;
        private CDACodifCode statusCode;
        private List<CDALabFactor> labFactors;
        private CDARemark remark;

        public string Name
        { get { return text.Value; } }

        public List<CDALabFactor> LabFactors
        { get { return labFactors; } }

        public CDALabFactorGroup(CDAText text, CDACodifCode statusCode, List<CDALabFactor> labFactors, CDARemark remark)
        {
            this.text = text;
            this.statusCode = statusCode;
            this.labFactors = labFactors;
            this.remark = remark;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", null, null, null, null, new CDAText(text.Value, "originalText")));
            elements.Add(statusCode);
            foreach (CDALabFactor labFactor in labFactors)
                elements.Add(new CDAContainer("component", labFactor));                
            if (remark != null)            
                elements.Add(new CDAContainer("component", remark));

            CDAContainer container = new CDAContainer("organizer", elements, new CDAElementAttribute("classCode", "BATTERY"), new CDAElementAttribute("moodCode", "EVN"));            
            return container.ToXmlElement(document);
        }

        public static CDALabFactorGroup Create(string id)
        {
            CDAText text = new CDAText("Клинический анализ крови");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            List<CDALabFactor> labFactors = new List<CDALabFactor>();
            labFactors.Add(CDALabFactor.Create("id лабораторного показателя"));
            CDARemark remark = new CDARemark(
                            new CDACodifCode("code", "900", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Текстовое примечание, заключение, комментарий, рекомендации к лабораторному исследованию"),
                            new CDAText("Примечание: обнаружение шизоцитов требует дополнительных микроскопических исследований"),
                            new CDAAuthor(CDAEmployeeInfo.CreateShort("2341"), new CDATime(new DateTime(2016, 09, 02, 11, 11, 00, DateTimeKind.Local), "time")));

            return new CDALabFactorGroup(text, statusCode, labFactors, remark);
        }

    }
}
