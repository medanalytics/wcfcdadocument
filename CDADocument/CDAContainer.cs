﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAContainer : ICDASerializable
    {
        private string Type;
        private List<CDAElementAttribute> Attributes;
        private List<ICDASerializable> Content;

        public CDAContainer(string Type, string TypeCode, params ICDASerializable[] Content)
        {
            this.Content = new List<ICDASerializable>();
            this.Type = Type;
            foreach (ICDASerializable element in Content)
                this.Content.Add(element);
            this.Attributes = new List<CDAElementAttribute>();
            if (TypeCode != null)
                Attributes.Add(new CDAElementAttribute("typeCode", TypeCode));
        }

        public CDAContainer(string Type, ICDASerializable Content, params CDAElementAttribute[] Attributes)
        {
            this.Content = new List<ICDASerializable>();
            this.Type = Type;
            this.Content.Add(Content);
            this.Attributes = new List<CDAElementAttribute>();
            foreach (CDAElementAttribute attribute in Attributes)
                this.Attributes.Add(attribute);
        }

        public CDAContainer(string Type, List<ICDASerializable> Content, params CDAElementAttribute[] Attributes)
        {
            this.Type = Type;
            this.Content = Content;
            this.Attributes = new List<CDAElementAttribute>();
            foreach (CDAElementAttribute attribute in Attributes)
                this.Attributes.Add(attribute);
        }

        public void AddComponent(ICDASerializable Component)
        {
            Content.Add(Component);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elContainer = document.CreateElement(Type);
            foreach (CDAElementAttribute attribute in Attributes)
                elContainer = attribute.ToXmlElementAtr(elContainer);
            foreach (ICDASerializable element in Content)
                elContainer.AppendChild(element.ToXmlElement(document));

            return elContainer;
        }
    }
}
