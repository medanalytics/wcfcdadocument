﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Морфологическое исследование
    public class CDAObservationMor : ICDASerializable
    {
        private CDACodifCode code;
        private CDACodifCode statusCode;
        private CDATime time;
        private CDACodifCode priorityCode;
        private CDAValue value;
        private CDAPerformer performer;
        private List<CDAService> services; // медицинские услуги
        private CDAExternalDocReference reference;        

        public string Date
        { get { return time.GetDate(); } }

        public string Name
        {
            get
            {
                if (code.GetDisplayName() != null)
                    return code.GetDisplayName();
                else
                {
                    string val = "";
                    foreach (CDAService service in services)
                        val = CDAString.ConcatWithSeparator(val, service.Name, "<br/>");
                    return val;
                }
            }
        }

        public string Result
        { get { return value.Value; } }

        public string Priority
        { get { return priorityCode.GetDisplayName(); } }

        public string Performer
        { get { return performer.GetPerformer(); } }

        public CDAObservationMor(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, CDAExternalDocReference reference, params CDAService[] services)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;            
            this.reference = reference;            
            this.services = new List<CDAService>();
            foreach (CDAService service in services)
                this.services.Add(service);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(code);
            elements.Add(statusCode);
            elements.Add(time);
            elements.Add(priorityCode);
            elements.Add(value);
            elements.Add(performer);
            foreach (CDAService service in services)
                elements.Add(new CDAContainer("entryRelationship", service, new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (reference != null)
                elements.Add(reference);
            
            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }

        public static CDAObservationMor CreateStac()
        {
            CDACodifCode code =  new CDACodifCode("code", "561", "1.2.643.5.1.13.2.1.1.1504.10", "Справочник типов лабораторных исследований", "Иммуногистохимическое исследование");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2014, 05, 15, 14, 30, 0, DateTimeKind.Local));            
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");
            
            CDAValue value = new CDAValue(@"Результаты и\или заключение");
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));            
            // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(
                new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754862"));

            return new CDAObservationMor(code: code,
                                         statusCode: statusCode,
                                         time: time,
                                         priorityCode: priorityCode,
                                         value: value,
                                         performer: performer,
                                         reference: reference);
        }

        public static CDAObservationMor CreateAmb()
        {
            CDACodifCode code = new CDACodifCode("code", new nullFlavor(nFValue.NA));
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2016, 10, 06, 12, 00, 0, DateTimeKind.Local));
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");

            CDAValue value = new CDAValue(@"Иммунофенотип опухоли не противоречит иммунофенотипу фибромиксоидной саркомы низкой степени злокачественности.");
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));
            // Медицинская услуга
            CDAService service = new CDAService(
                                new CDACodifCode("code", "A08.30.013", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Иммуногистохимическое исследование материала"),
                                new CDATime(new DateTime(2016, 10, 06, 12, 00, 00, DateTimeKind.Local)));
            // Ссылка на исходный документ исследования
            CDAExternalDocReference reference = new CDAExternalDocReference(
                new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "8754862"));

            return new CDAObservationMor(code: code,
                                         statusCode: statusCode,
                                         time: time,
                                         priorityCode: priorityCode,
                                         value: value,
                                         performer: performer,
                                         reference: reference,
                                         services: service);
        }
    }
}
