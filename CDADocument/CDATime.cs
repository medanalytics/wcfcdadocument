﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Время
    public class CDATime : ICDASerializable
    {
        private string Name; // имя xml-элемента со временем (time, effectiveTime и др.) 
        private DateTime low; // если DateTimeKind.Local, то будет выводиться дата со временем
        private DateTime high; // иначе - только дата
        private bool isInterval; // признак того, что задан интервал времени

        public DateTime Low
        { get { return low; } }

        public DateTime High
        { get { return high; } }

        // Задаем время
        public CDATime(DateTime Value)
        {
            Name = "effectiveTime";
            low = Value;
            isInterval = false;
        }
        // Задаем время и название xml-элемента        
        public CDATime(DateTime Value, string Name)
        {
            this.Name = Name;
            this.low = Value;
            isInterval = false;
        }
        // Задаем временной интервал
        public CDATime(DateTime Low, DateTime High)
        {
            Name = "effectiveTime";
            this.low = Low;
            this.high = High;
            isInterval = true;
        }

        public override string ToString()
        {
            if (isInterval)
                return "с " + low.ToString("dd.MM.yyyy") + " по " + high.ToString("dd.MM.yyyy");
            else
                return low.ToString("dd.MM.yyyy");
        }

        public string GetDate()
        {
            if (!isInterval)
                return low.ToString("dd.MM.yyyy");
            else
                return null;
        }

        public string GetTime()
        {
            if (!isInterval)
                return low.ToString("hh:mm");
            else
                return null;
        }
        // Преобразуем в текстовую строку по требованиям CDA
        private string ToCDAString(DateTime date)
        {
            if (date.Kind == DateTimeKind.Local)
                // Если указано местное время, то выводим не только дату, но и время
                return date.ToString("yyyyMMddhhmmK").Replace(":", "");
            else
                return date.ToString("yyyyMMdd");
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elTime = document.CreateElement(Name);
            if (!isInterval)
                elTime.SetAttribute("value", ToCDAString(low));
            else
            {
                if (low != default(DateTime))
                {
                    XmlElement elLow = document.CreateElement("low");
                    elLow.SetAttribute("value", ToCDAString(low));
                    elTime.AppendChild(elLow);
                }
                if (high != default(DateTime))
                {
                    XmlElement elHigh = document.CreateElement("high");
                    elHigh.SetAttribute("value", ToCDAString(high));
                    elTime.AppendChild(elHigh);
                }
            }

            return elTime;
        }
    }
}
