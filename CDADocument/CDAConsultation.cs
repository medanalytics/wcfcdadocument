﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAConsultation : ICDASerializable
    {
        CDACodifCode code;
        CDACodifCode statusCode;
        CDATime time;
        CDACodifCode priorityCode;
        CDAValue value;
        CDAPerformer performer;
        List<CDAService> services;
        CDACodeVal condition;
        CDACodeVal protocol;
        CDACodeVal conclusion;
        CDACodeVal recomendation;
        List<CDACodeVal> pathologies;
        CDACodeVal diagnosis;
        CDACodeVal result;
        CDAExternalDocReference reference;

        public string Time
        { get { return time.GetDate() + "<br/>" + time.GetTime(); } }

        public string Job
        { get { return performer.GetJob(); } }

        public string Performer
        { get { return performer.GetName(); } }

        public CDAConsultation(CDACodifCode code, CDACodifCode statusCode, CDATime time, CDACodifCode priorityCode,
                    CDAValue value, CDAPerformer performer, List<CDAService> services, CDACodeVal condition,
                    CDACodeVal protocol, CDACodeVal conclusion, CDACodeVal recomendation, List<CDACodeVal> pathologies,
                    CDACodeVal diagnosis, CDACodeVal result, CDAExternalDocReference reference)
        {
            this.code = code;
            this.statusCode = statusCode;
            this.time = time;
            this.priorityCode = priorityCode;
            this.value = value;
            this.performer = performer;
            this.services = services;
            this.condition = condition;
            this.protocol = protocol;
            this.conclusion = conclusion;
            this.recomendation = recomendation;
            this.pathologies = pathologies;
            this.diagnosis = diagnosis;
            this.result = result;
            this.reference = reference;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(code);
            elements.Add(statusCode);
            elements.Add(time);
            elements.Add(priorityCode);
            elements.Add(value);
            elements.Add(performer);
            foreach (CDAService service in services)
                elements.Add(new CDAContainer("entryRelationship", service, new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            elements.Add(new CDAContainer("entryRelationship", "COMP", condition));
            elements.Add(new CDAContainer("entryRelationship", "COMP", protocol));
            elements.Add(new CDAContainer("entryRelationship", "COMP", conclusion));
            if (recomendation != null)
                elements.Add(new CDAContainer("entryRelationship", "COMP", recomendation));
            foreach (CDACodeVal pathology in pathologies)
                elements.Add(new CDAContainer("entryRelationship", "COMP", pathology));
            elements.Add(new CDAContainer("entryRelationship", "COMP", diagnosis));
            if (result != null)
                elements.Add(new CDAContainer("entryRelationship", "COMP", result));
            if (reference != null)
                elements.Add(reference);

            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);
        }

        public string GenerateProtocol(CDATextReferenceManager refMan)
        {
            CDAText text = new CDAText();
            // Состояние
            CDATextParagraph parCondition = text.AddParagraph();
            parCondition.AddCaption("Состояние");
            CDATextReference refCondition = refMan.GetTextReference();
            parCondition.AddXmlElement(text.CreateTextWithReference(refCondition, condition.Code.GetDisplayName()));
            condition.Code.SetReference(refCondition);
            // Протокол
            CDATextParagraph parProtocol = text.AddParagraph();
            parProtocol.AddCaption("Протокол");
            CDATextReference refProtocol = refMan.GetTextReference();
            parProtocol.AddXmlElement(text.CreateTextWithReference(refProtocol, protocol.Value.Value));
            protocol.SetReference(refProtocol);
            // Заключение
            CDATextParagraph parConclusion = text.AddParagraph();
            parConclusion.AddCaption("Заключение");
            CDATextReference refConclusion = refMan.GetTextReference();
            parConclusion.AddXmlElement(text.CreateTextWithReference(refConclusion, conclusion.Value.Value));
            conclusion.SetReference(refConclusion);
            // Рекомендации
            if (recomendation != null)
            {
                CDATextParagraph parRecomend = text.AddParagraph();
                parRecomend.AddCaption("Рекомендации");
                CDATextReference refRecomend = refMan.GetTextReference();
                parRecomend.AddXmlElement(text.CreateTextWithReference(refRecomend, recomendation.Value.Value));
                recomendation.SetReference(refRecomend);
            }
            // Выявленные патологии
            if (pathologies.Count > 0)
            {
                CDATextParagraph parPat = text.AddParagraph();
                parPat.AddCaption("Выявленные патологии");
                foreach (CDACodeVal pathology in pathologies)
                {
                    CDATextReference refPat = refMan.GetTextReference();
                    parPat.AddXmlElement(text.CreateTextWithReference(refPat, pathology.Value.Value));
                    pathology.Value.SetReference(refPat);
                }
            }
            // Шифр по МКБ-10
            CDATextParagraph parDiag = text.AddParagraph();
            parDiag.AddCaption("Шифр по МКБ-10");
            CDATextReference refDiag = refMan.GetTextReference();
            parDiag.AddXmlElement(text.CreateTextWithReference(refDiag, diagnosis.Value.Value));
            diagnosis.Value.SetReference(refDiag);
            // Результат консультации
            CDATextParagraph parResult = text.AddParagraph();
            parResult.AddCaption("Результат консультации");
            CDATextReference refResult = refMan.GetTextReference();
            parResult.AddXmlElement(text.CreateTextWithReference(refResult, result.Value.Value));
            result.Value.SetReference(refResult);

            return text.Value;
        }

        public static CDAConsultation Create()
        {
            CDACodifCode code = new CDACodifCode("code", "118", "1.2.643.5.1.13.13.11.1066", "Номенклатура специальностей специалистов со средним, высшим и послевузовским медицинским и фармацевтическим образованием в сфере здравоохранения", "Кардиология");
            CDACodifCode statusCode = new CDACodifCode("statusCode", "completed", null, null, null);
            CDATime time = new CDATime(new DateTime(2016, 10, 07, 07, 07, 00, DateTimeKind.Local));
            CDACodifCode priorityCode = new CDACodifCode("priorityCode", "0", "1.2.643.5.1.13.2.1.1.1504.6", "Справочник приоритетов", "Низкий приоритет");
            CDAValue value = new CDAValue("На момент осмотра - жалоб нет, боли в области сердца и одышку отрицает. Последний раз ЭКГ делал три года назад - об аритмии нечего не знает и сам нарушений ритма не ощущает.");
            CDAPerformer performer = new CDAPerformer(CDAEmployeeInfo.CreateShort("2343"));
            List<CDAService> services = new List<CDAService>();
            services.Add(new CDAService(
                                new CDACodifCode("code", "B01.015.001", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Прием (осмотр, консультация) врача-кардиолога первичный"),
                                new CDATime(new DateTime(2016, 10, 07, 07, 07, 00, DateTimeKind.Local))));
            CDACodeVal condition = new CDACodeVal(new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1006", "Степень тяжести состояния пациента", "Удовлетворительное"));
            CDACodeVal protocol = new CDACodeVal(
                                new CDACodifCode("code", "805", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Протокол консультации"),
                                new CDAValue("Произвольный текст протокола"));
            CDACodeVal conclusion = new CDACodeVal(
                                new CDACodifCode("code", "806", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Заключение консультации"),
                                new CDAValue("Произвольный текст заключения"));
            CDACodeVal recomendation = new CDACodeVal(
                                new CDACodifCode("code", "807", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Рекомендации"),
                                new CDAValue("Произвольный текст рекомендаций"));
            List<CDACodeVal> pathologies = new List<CDACodeVal>();
            pathologies.Add(new CDACodeVal(
                                new CDACodifCode("code", "808", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Выявленные патологии"),
                                new CDAValue(new CDACodifCode("R00.0", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Тахикардия неуточненная"))));
            CDACodeVal diagnosis = new CDACodeVal(
                                new CDACodifCode("code", "809", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Шифр по МКБ-10"),
                                new CDAValue(new CDACodifCode("I11.9", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Гипертоническая болезнь сердца")));
            CDACodeVal result = new CDACodeVal(
                                new CDACodifCode("code", "810", "1.2.643.5.1.13.2.1.1.1504.41", "Справочник кодируемых полей", "Результат консультации"),
                                new CDAValue(new CDACodifCode("1", "1.2.643.5.1.13.13.11.1009", "Справочник видов медицинских направлений", "На плановую госпитализацию")));
            CDAExternalDocReference reference = new CDAExternalDocReference(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.231.100.1.1.51", "8754859"));

            return new CDAConsultation(code: code,
                                       statusCode: statusCode,
                                       time: time,
                                       priorityCode: priorityCode,
                                       value: value,
                                       performer: performer,
                                       services: services,
                                       condition: condition,
                                       protocol: protocol,
                                       conclusion: conclusion,
                                       recomendation: recomendation,
                                       pathologies: pathologies,
                                       diagnosis: diagnosis,
                                       result: result,
                                       reference: reference);

        }
    }
}
