﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDAExternalDocReference : ICDASerializable
    {
        private CDARootExt id;

        public CDAExternalDocReference(CDARootExt id)
        {
            this.id = id;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("reference", "REFR",
                                         new CDAContainer("externalDocument",
                                             id));
            return container.ToXmlElement(document);
        }
    }
}
