﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Тип анестезии
    public class CDAAnesthetic : ICDASerializable
    {
        private CDACodifCode code;

        public CDACodifCode Code
        { get { return code; } }

        public CDAAnesthetic(CDACodifCode code)
        {
            this.code = code;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("entryRelationship", "COMP",
                                         new CDAContainer("substanceAdministration",
                                             new CDAContainer("consumable", "CSM",
                                                 new CDAContainer("manufacturedProduct",
                                                     new CDAContainer("manufacturedLabeledDrug",
                                                         code),
                                                     new CDAElementAttribute("classCode", "MANU"))),
                                             new CDAElementAttribute("classCode", "SBADM"),
                                             new CDAElementAttribute("moodCode", "EVN")));
            return container.ToXmlElement(document);
        }
    }
}
