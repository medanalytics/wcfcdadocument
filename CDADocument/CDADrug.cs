﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDADrug : ICDASerializable
    {
        private CDACodifCode code;

        public string Name
        { get { return code.GetDisplayName(); } }

        public CDADrug(CDACodifCode code)
        {
            this.code = code;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("substanceAdministration",
                                        new CDAContainer("consumable",
                                            new CDAContainer("manufacturedProduct",
                                                new CDAContainer("manufacturedLabeledDrug",
                                                    code))),
                                        new CDAElementAttribute("classCode", "SBADM"),
                                        new CDAElementAttribute("moodCode", "EVN"));
            return container.ToXmlElement(document);
        }
    }
}
