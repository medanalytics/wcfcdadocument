﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Контакты
    public class CDATelecom : ICDASerializable
    {
        // Вид контакта (рабочий/мобильный телефон)
        public enum Kind
        { Unspecified, WP, MC }

        // URL способа связи
        private string Contact;
        // Мобильный/домашний телефон
        private Kind Use;
        private nullFlavor nF;

        public CDATelecom(string Contact, Kind Use)
        {
            this.Contact = Contact;
            this.Use = Use;
            nF = null;
        }

        public CDATelecom(string Contact)
        {
            this.Contact = Contact;
            this.Use = Kind.Unspecified;
            nF = null;
        }

        public CDATelecom(nullFlavor nf)
        {
            this.nF = nf;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elTelecom = document.CreateElement("telecom");
            if (nF != null)
                elTelecom = nF.ToXmlElementAtr(elTelecom);
            else
            {
                if (Use != Kind.Unspecified)
                    elTelecom.SetAttribute("use", Use.ToString());
                elTelecom.SetAttribute("value", Contact);
            }
            return elTelecom;
        }
    }
}
