﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    public class CDADocument
    {
        private string urn;
        private string schema;
        private string stylesheet;
        private List<ICDASerializable> elements;

        public CDADocument(string urn, string schema, string stylesheet, List<ICDASerializable> elements)
        {
            this.urn = urn;
            this.schema = schema;
            this.stylesheet = stylesheet;
            this.elements = elements;
        }

        public XmlDocument ToXmlDocument()
        {
            XmlDocument document = new XmlDocument();
            XmlDeclaration declaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);
            document.AppendChild(declaration);
            document.AppendChild(document.CreateProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + stylesheet + "\""));

            XmlElement root = document.CreateElement("ClinicalDocument");
            root.SetAttribute("xmlns", urn);
            root.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            XmlAttribute attr = document.CreateAttribute("schemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            attr.Value = urn + " " + schema;            
            root.Attributes.Append(attr);

            foreach (ICDASerializable element in elements)
                root.AppendChild(element.ToXmlElement(document));

            document.AppendChild(root);

            return document;
        }
    }
}
