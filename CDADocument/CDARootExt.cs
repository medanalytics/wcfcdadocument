﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // элементы с атрибутами root и extension
    public class CDARootExt : ICDASerializable
    {
        private string Root;
        private string Extension;
        private string Name;

        public CDARootExt(string Root, string Extension)
        {
            this.Root = Root;
            this.Extension = Extension;
            Name = "id";
        }

        public CDARootExt(string Name, string Root, string Extension)
        {
            this.Root = Root;
            this.Extension = Extension;
            this.Name = Name;
        }

        public XmlElement ToXmlElementAtr(XmlElement element)
        {
            if (Root != null)
                element.SetAttribute("root", Root);
            if (Extension != null)
                element.SetAttribute("extension", Extension);
            return element;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            if (Name != null)
                return ToXmlElementAtr(document.CreateElement(Name));
            else
                return null;
        }
    }
}
