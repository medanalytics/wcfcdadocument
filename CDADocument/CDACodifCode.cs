﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // элементы с атрибутами code, codeSystem и т.д.
    public class CDACodifCode : ICDASerializable
    {
        private string Code;
        private string CodeSystem;
        private string CodeSystemName;
        private string DisplayName;
        private string Name;
        private nullFlavor nf;
        private CDATextReference reference; // ссылка на текстовую часть секции
        private List<ICDASerializable> Components; // список с элементами qualifier и translation

        public CDATextReference Reference
        { get { return reference; } }

        public CDACodifCode(string Code, string CodeSystem, string CodeSystemName, string DisplayName)
        {
            this.Code = Code;
            this.CodeSystem = CodeSystem;
            this.CodeSystemName = CodeSystemName;
            this.DisplayName = DisplayName;
            Name = "code";
            nf = null;
            reference = null;
            Components = new List<ICDASerializable>();
        }

        public CDACodifCode(string Name, string Code, string CodeSystem, string CodeSystemName, string DisplayName, params ICDASerializable[] Components)
        {
            this.Code = Code;
            this.CodeSystem = CodeSystem;
            this.CodeSystemName = CodeSystemName;
            this.DisplayName = DisplayName;
            this.Name = Name;
            nf = null;
            reference = null;
            this.Components = new List<ICDASerializable>();
            foreach (ICDASerializable component in Components)
                this.Components.Add(component);

        }

        public CDACodifCode(string Name, nullFlavor nf)
        {
            this.Name = Name;
            this.nf = nf;
            Code = null;
            CodeSystem = null;
            CodeSystemName = null;
            DisplayName = null;
            Components = new List<ICDASerializable>();
            reference = null;
        }

        public void AddComponent(ICDASerializable component)
        {
            Components.Add(component);
        }

        public void SetReference(CDATextReference reference)
        {
            this.reference = reference;
        }

        public string GetCode()
        {
            return Code;
        }

        public string GetDisplayName()
        {
            return DisplayName;
        }

        public XmlElement ToXmlElementAtr(XmlElement element)
        {
            if (Code != null)
                element.SetAttribute("code", Code);
            if (CodeSystem != null)
                element.SetAttribute("codeSystem", CodeSystem);
            if (CodeSystemName != null)
                element.SetAttribute("codeSystemName", CodeSystemName);
            if (DisplayName != null)
                element.SetAttribute("displayName", DisplayName);
            if (nf != null)
                element = nf.ToXmlElementAtr(element);
            return element;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elCode = document.CreateElement(Name);
            elCode = ToXmlElementAtr(elCode);
            if (reference != null)
                elCode.AppendChild(reference.ToXmlElement(document));
            foreach (ICDASerializable component in Components)
                elCode.AppendChild(component.ToXmlElement(document));

            return elCode;
        }
    }
}
