﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Витальный параметр
    public class CDAVitalParameter : ICDASerializable
    {
        private CDACodifCode code;
        private CDAValue value;

        public CDACodifCode Code
        { get { return code; } }

        public CDAValue Value
        { get { return value; } }

        public CDAVitalParameter(CDACodifCode code, CDAValue value)
        {
            this.code = code;
            this.value = value;
        }

        public void AddTextReference(CDATextReference reference)
        {
            code.AddComponent(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer component = new CDAContainer("component", "COMP", CDAActStatement.CreateObservation(code, value));
            return component.ToXmlElement(document);
        }
    }
}
