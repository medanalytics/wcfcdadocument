﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // реализуем строки с возможностью хранения значений nullFlavor
    public class CDAString
    {
        private string value; // значение строки
        private nullFlavor nF; // значение nullFlavor

        public string Value
        { get { return value; } }

        public CDAString(string value)
        {
            this.value = value;
            this.nF = null;
        }

        public CDAString(nullFlavor nF)
        {
            this.value = null;
            this.nF = nF;
        }

        public XmlElement ToXmlElementVal(XmlElement element)
        {
            if (nF != null)
                element = nF.ToXmlElementAtr(element);
            else if (value != null)
                element.InnerText = value;
            return element;
        }

        public static string ConcatWithSeparator(string first, string second, string separator)
        {
            if (first == null || second == null || separator == null)
                return null;
            else if (first != "")
                return first + separator + second;
            else
                return second;
        }
    }
}
