﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Данные о медицинском полисе
    public class CDAPolis : ICDASerializable
    {
        private CDARootExt Number;
        private CDACodifCode Holder;
        private CDAOrganisation Owner;

        public CDAPolis(CDARootExt Number, CDAOrganisation Owner)
        {
            this.Number = Number;
            // Будем считать, что держателем полиса ОМС является сам пациент
            this.Holder = new CDACodifCode("SELF", "2.16.840.1.113883.5.111", null, null);
            this.Owner = Owner;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elParticipant = document.CreateElement("participant");
            elParticipant.SetAttribute("typeCode", "HLD");
            XmlElement elEntity = document.CreateElement("associatedEntity");
            elEntity.SetAttribute("classCode", "POLHOLD");
            elEntity.AppendChild(Number.ToXmlElementAtr(document.CreateElement("id")));
            elEntity.AppendChild(Holder.ToXmlElementAtr(document.CreateElement("code")));
            XmlElement elOrg = document.CreateElement("scopingOrganization");
            foreach (XmlElement element in Owner.ToXmlElements(document))
                elOrg.AppendChild(element);
            elEntity.AppendChild(elOrg);
            elParticipant.AppendChild(elEntity);
            return elParticipant;
        }

        public static CDAPolis Create()
        // В реальном случае информация о полисе зависит от id пациента и
        // периода случая оказания медицинской помощи, но здесь мы этим пренебрежем.
        {
            return new CDAPolis(new CDARootExt("1.2.643.5.1.13.2.7.100.2", "1234561234567890"), CDAOrganisation.CreateSMO("11"));
        }
    }
}
