﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Организация - владелец документа
    public class CDACustodian : ICDASerializable
    {
        private CDAOrganisation Custodian;

        public CDACustodian(CDAOrganisation Custodian)
        {
            this.Custodian = Custodian;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elCustodian = document.CreateElement("custodian");
            XmlElement elAssigned = document.CreateElement("assignedCustodian");
            XmlElement elRepresented = document.CreateElement("representedCustodianOrganization");
            foreach (XmlElement element in Custodian.ToXmlElements(document))
                elRepresented.AppendChild(element);
            elAssigned.AppendChild(elRepresented);
            elCustodian.AppendChild(elAssigned);

            return elCustodian;
        }
    }
}
