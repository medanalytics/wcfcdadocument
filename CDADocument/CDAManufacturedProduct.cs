﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Медицинские устройства или импланты
    public class CDAManufacturedProduct : ICDASerializable
    {
        private CDACodifCode code;

        public CDACodifCode Code
        { get { return code; } }

        public CDAManufacturedProduct(CDACodifCode code)
        {
            this.code = code;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("participant", "DEV",
                                         new CDAContainer("participantRole",
                                             new CDAContainer("playingDevice", code),
                                             new CDAElementAttribute("classCode", "MANU")));
            return container.ToXmlElement(document);
        }
    }
}
