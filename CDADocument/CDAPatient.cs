﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Информация о пациенте
    public class CDAPatient : ICDASerializable
    {
        // ID в МИС, СНИЛС, паспорт и т.д.
        private List<CDARootExt> Ids;
        // ФИО
        private CDAName Name;
        // Дата рождения
        private CDABirthTime Birthday;
        // Пол
        private CDACodifCode Gender;
        // Список адресов
        private List<CDAAddress> Addresses;
        // Список контактов
        private List<CDATelecom> Contacts;
        // Организация, оказавшая помощь
        private CDAOrganisation Organisation;

        public CDAPatient(List<CDARootExt> Ids, CDAName Name, CDABirthTime Birthday, string Snils, CDACodifCode Gender, List<CDAAddress> Addresses, List<CDATelecom> Contacts, CDAOrganisation Organisation)
        {
            this.Ids = Ids;
            this.Name = Name;
            this.Birthday = Birthday;
            this.Gender = Gender;
            this.Addresses = Addresses;
            this.Contacts = Contacts;
            this.Organisation = Organisation;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elTarget = document.CreateElement("recordTarget");
            XmlElement elPatRole = document.CreateElement("patientRole");

            // список id пациента
            foreach (CDARootExt id in Ids)
                elPatRole.AppendChild(id.ToXmlElementAtr(document.CreateElement("id")));
            // Адреса
            foreach (CDAAddress address in Addresses)
                elPatRole.AppendChild(address.ToXmlElement(document));
            // Телефоны
            foreach (CDATelecom contact in Contacts)
                elPatRole.AppendChild(contact.ToXmlElement(document));
            // Секция <patient>
            XmlElement elPatient = document.CreateElement("patient");
            // добавляем <patient> в документ
            elPatRole.AppendChild(elPatient);
            // ФИО
            elPatient.AppendChild(Name.ToXmlElement(document));
            // Пол            
            elPatient.AppendChild(Gender.ToXmlElementAtr(document.CreateElement("administrativeGenderCode")));
            // Дата рождения           
            elPatient.AppendChild(Birthday.ToXmlElement(document));
            // Мед. организация
            XmlElement elOrg = document.CreateElement("providerOrganization");
            foreach (XmlElement element in Organisation.ToXmlElements(document))
                elOrg.AppendChild(element);
            elPatRole.AppendChild(elOrg);

            elTarget.AppendChild(elPatRole);

            return elTarget;
        }

        public static CDAPatient Create(string id)
        // Предполагается, что этот метод будет искать в БД МИС
        // информацию о пациенте по его id и передавать ее в конструктор.
        {
            CDAName Name = new CDAName(new CDAString("Иванов"), new CDAString("Иван"), new CDAString("Иванович"));
            CDABirthTime Birthday = new CDABirthTime(new DateTime(1980, 5, 15));
            // Идентификаторы пациента
            List<CDARootExt> Ids = new List<CDARootExt>();
            Ids.Add(new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.10", id)); // идентификатор в МИС
            Ids.Add(new CDARootExt("1.2.643.100.3", "445-784-445 10")); // СНИЛС
            // Пол
            CDACodifCode Gender = new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.156", "Классификатор половой принадлежности", "Мужской");
            //Адрес регистрации
            List<CDAAddress> Addresses = new List<CDAAddress>();
            Addresses.Add(new CDAAddress(new CDAString("194293, г. Санкт-Петербург, ул. Ленина, д. 5, кв. 10"), new CDAString("78"), CDAAddress.Kind.H));
            //Адрес места жительства
            Addresses.Add(new CDAAddress(new CDAString("194293, г. Санкт-Петербург, ул. Ленина, д. 5, кв. 10"), new CDAString("78"), CDAAddress.Kind.HP));
            //Контакты
            List<CDATelecom> Contacts = new List<CDATelecom>();
            Contacts.Add(new CDATelecom("tel:+78121234567"));
            Contacts.Add(new CDATelecom("tel:+79117654321", CDATelecom.Kind.MC));
            Contacts.Add(new CDATelecom("mailto:ivanovii1980@yandex.ru"));
            //Мед. организация
            CDAOrganisation Organisation = CDAOrganisation.CreateMO("1.2.643.5.1.13.3.25.77.923");

            return new CDAPatient(Ids: Ids, Name: Name, Birthday: Birthday, Snils: "445-784-445 10", Gender: Gender,
                                        Addresses: Addresses, Contacts: Contacts, Organisation: Organisation);
        }
    }
}
