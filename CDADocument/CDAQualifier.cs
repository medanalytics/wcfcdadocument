﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // уточняющий элемент для CDACodifCode
    public class CDAQualifier : ICDASerializable
    {
        ICDASerializable[] Components;

        public CDAQualifier(params ICDASerializable[] Components)
        {
            this.Components = Components;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elQualifier = document.CreateElement("qualifier");
            foreach (ICDASerializable component in Components)
                elQualifier.AppendChild(component.ToXmlElement(document));

            return elQualifier;
        }
    }
}
