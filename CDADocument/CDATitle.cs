﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Заголовок секции
    public class CDATitle : ICDASerializable
    {
        private string Title;

        public CDATitle(string Title)
        {
            this.Title = Title;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elTitle = document.CreateElement("title");
            elTitle.InnerText = Title;
            return elTitle;
        }
    }
}
