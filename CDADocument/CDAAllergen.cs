﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Аллерген
    public class CDAAllergen : ICDASerializable
    {
        private CDACodifCode code; // аллерген-медикамент
        private string desc; // описание аллергена-не медикамента
        private AllergenType type; // тип аллергена

        private string roleTypeCode;
        private string entityTypeCode;

        public string Type
        {
            get
            {
                if (type == AllergenType.Drug)
                    return "медикамент";
                else
                    return "не медикамент";
            }
        }

        public string Agent
        {
            get
            {
                if (type == AllergenType.Drug)
                    return code.GetDisplayName();
                else
                    return desc;
            }
        }

        enum AllergenType
        {
            Drug,
            Other
        }

        // аллерген - медикамент
        public CDAAllergen(CDACodifCode code)
        {
            this.code = code;
            type = AllergenType.Drug;
            desc = null;

            roleTypeCode = "MANU";
            entityTypeCode = "MMAT";
        }

        // аллерген - не медикамент
        public CDAAllergen(string desc)
        {
            this.desc = desc;
            type = AllergenType.Other;
            code = new CDACodifCode("code", new nullFlavor(nFValue.NA));

            roleTypeCode = "SPEC";
            entityTypeCode = "MAT";
        }

        public void AddTextReference(CDATextReference reference)
        {
            code.AddComponent(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer entity = new CDAContainer("playingEntity", code, new CDAElementAttribute("classCode", entityTypeCode));
            if (type == AllergenType.Other)
                entity.AddComponent(new CDAText(desc, "desc"));
            CDAContainer allergen = new CDAContainer("participant", "IND", new CDAContainer("participantRole", entity, new CDAElementAttribute("classCode", roleTypeCode)));
            return allergen.ToXmlElement(document);
        }
    }
}
