﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // ссылка на фрагмент текстовой части секции
    public class CDATextReference : ICDASerializable
    {
        private string value;
        private string name;

        public string Value
        {
            get { return value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public CDATextReference(string value)
        {
            this.value = value;
            name = "originalText";
        }

        // возвращает строку content, обрамленную элементом <content>
        public string GetContentXml(string content)
        {
            string xml = "<content ID=\"" + value + "\">" + content + "</content>";
            return xml;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elOrigText = document.CreateElement(name);
            XmlElement elReference = document.CreateElement("reference");
            elReference.SetAttribute("value", "#" + value);
            elOrigText.AppendChild(elReference);

            return elOrigText;
        }
    }
}
