﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Количество (величина с единицами измерения)
    public class CDAQuantity : ICDASerializable
    {
        private string value;
        private string unit;

        public string Value
        { get { return value; } }

        public string Unit
        { get { return unit; } }

        public CDAQuantity(string value, string unit)
        {
            this.value = value;
            this.unit = unit;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elQuantity = document.CreateElement("quantity");
            elQuantity.SetAttribute("value", value);
            elQuantity.SetAttribute("unit", unit);

            return elQuantity;
        }

        public XmlElement ToXmlElementAtr(XmlElement element)
        {
            element.SetAttribute("value", value);
            element.SetAttribute("unit", unit);

            return element;
        }
    }
}
