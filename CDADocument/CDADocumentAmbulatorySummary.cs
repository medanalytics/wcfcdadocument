﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDADocumentClasses
{
    public static class CDADocumentAmbulatorySummary
    {
        public static CDADocument Create()
        {
            // Данные для заголовка xml-документа
            string urn = "urn:hl7-org:v3";
            string schema = "http://tech-iemc-test.rosminzdrav.ru/tech/download/infrastructure/2/CDA.xsd";
            string stylesheet = "http://tech-iemc-test.rosminzdrav.ru/tech/download/XSLT/2/AmbSum.xsl";

            // Значения, одинаковые для всех отчетов
            CDACodifCode RealmCode = new CDACodifCode("realmCode", "RU", null, null, null);
            CDARootExt typeId = new CDARootExt("typeId", "2.16.840.1.113883.1.3", "POCD_HD000040");
            CDACodifCode ConfidCode = new CDACodifCode("confidentialityCode", "N", "1.2.643.5.1.13.2.1.1.1504.9", "Уровень конфиденциальности документа", "Обычный");
            CDACodifCode LangCode = new CDACodifCode("languageCode", "ru-RU", null, null, null);
            CDARecipient Recipient = new CDARecipient(new CDAOrganisation(new CDARootExt("1.2.643.5.1.13", null), new CDAString("Министерство здравоохранения Российской Федерации (ИЭМК)"), null, new List<CDATelecom>()));
            // Список элементов документа
            List<ICDASerializable> elements = new List<ICDASerializable>();

            // Остальные значения берутся из БД
            elements.Add(RealmCode);
            elements.Add(typeId);
            elements.Add(new CDARootExt("templateId", "1.2.643.5.1.13.2.7.5.1.2.3", null));
            // Идентификатор документа
            elements.Add(new CDARootExt("id", "1.2.643.5.1.13.3.25.77.923.100.1.1.51", "7854321"));
            // Тип документа
            elements.Add(new CDACodifCode("code", "2", "1.2.643.5.1.13.2.1.1.646", "Система электронных медицинских документов", "Эпикриз по законченному случаю амбулаторный"));
            // Заголовок документа
            elements.Add(new CDATitle("Эпикриз по законченному случаю амбулаторный"));
            // Время создания документа
            elements.Add(new CDATime(new DateTime(2017, 08, 31, 16, 00, 00, DateTimeKind.Local)));
            // Уровень конфиденциальности
            elements.Add(ConfidCode);
            // Язык
            elements.Add(LangCode);
            // Идентификатор набора версий
            elements.Add(new CDARootExt("setId", "1.2.643.5.1.13.3.25.77.923.100.1.1.50", "7854321"));
            // Номер версии
            elements.Add(new CDAVersionNumber("1"));
            // Пациент
            elements.Add(CDAPatient.Create("585233"));
            // Автор документа
            elements.Add(CDAAuthor.Create());
            // Организация-владелец
            elements.Add(new CDACustodian(CDAOrganisation.CreateCustodian("1.2.643.5.1.13.3.25.77.923")));
            // Получатель
            elements.Add(Recipient);
            // Лицо, придавшее юридическую силу
            elements.Add(CDASignature.Create());
            // Полис ОМС
            elements.Add(CDAPolis.Create());
            // Сведения о случае оказания медицинской помощи
            elements.Add(CDAEncompassingEncounter.Create());
            // Добавим тело документа
            elements.Add(new CDAContainer("component", CreateBody()));
            return new CDADocument(urn, schema, stylesheet, elements);
        }

        public static CDADocumentBody CreateBody()
        {
            List<CDADocumentSection> sections = new List<CDADocumentSection>();
            sections.Add(CreateSectionAMBS());
            sections.Add(CreateSectionSOCANAM());
            sections.Add(CreateSectionAMBSV());
            sections.Add(CreateSectionALL());
            sections.Add(CreateSectionVITALPARAM());
            sections.Add(CreateSectionPROC());
            sections.Add(CreateSectionIMM());
            sections.Add(CreateSectionSUM());
            sections.Add(CreateSectionSUR());
            sections.Add(CreateSectionPROCEDURE());

            return new CDADocumentBody(sections);
        }

        public static CDADocumentSection CreateSectionAMBS()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "AMBS", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Сведения амбулаторно-поликлинического обращения"));
            elements.Add(new CDATitle("ОБРАЩЕНИЕ"));
            // Создадим информацию о диагнозах и текстовое наполнение секции
            CDAText Text = new CDAText();
            elements.Add(Text);
            // Информация о случае
            CDAEncounter encounter = CDAEncounter.Create();
            elements.Add(new CDAContainer("entry", encounter));
            // Результат обращения
            CDACodifCode result = new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1046", "Результаты обращения (госпитализации)", "Выздоровление");
            elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(result)));
            // Направление по завершении обращения
            CDACodifCode direct = new CDACodifCode("code", "7", "1.2.643.5.1.13.13.11.1009", "Виды медицинских направлений", "На санаторно-курортное лечение");
            elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(direct)));
            // Вид оплаты
            CDACodifCode payType = new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1039", "Источники оплаты медицинской помощи", "ОМС");
            elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(payType)));
            // Вид оказанной помощи в рамках обращения
            CDACodifCode medType = new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1034", "Виды медицинской помощи", "Первичная специализированная медицинская помощь");
            elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(medType)));
            // Добавим в текстовую часть общую информацию о случае            
            Text.AddParagraph("<caption>Шифр МКБ</caption>" + encounter.Diagnosis);
            // Таблица с заданной шириной столбцов
            CDATextTable encTable = Text.AddTable(null, 10, 20, 10, 20, 15, 25);
            encTable.AddHeader("Тип", "Цель", "Вид оплаты", "Даты", "Результат обращения", "Выдано направление");
            encTable.AddRow(encounter.Order, "по заболеванию", payType.GetDisplayName(), encounter.Period, result.GetDisplayName(), direct.GetDisplayName());
            // Список с диагнозами
            List<CDADiagnosis> lstDiag = new List<CDADiagnosis>();
            lstDiag.Add(CDADiagnosis.CreatePre()); // Предварительный диагноз
            lstDiag.Add(CDADiagnosis.CreateFinal()); // Заключительный жиагноз
            foreach (CDADiagnosis diag in lstDiag)
            {
                // Добавим в текстовую часть таблицу с диагнозом с новой строки
                Text.AddBr();
                CDATextTable tblDiag = Text.AddTable(diag.Type, 10, 20, 70);
                tblDiag.AddHeader("Шифр", "Тип", "Текст");
                // Пройдемся по компонентам диагноза и добавим информацию из них в таблицу
                foreach (CDADiagnosisInfo component in diag.Components)
                    tblDiag.AddRow(component.Code, component.Type, component.Text);
                // Добавим диагноз в список элементов
                elements.Add(new CDAContainer("entry", diag));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSOCANAM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            // Код секции
            elements.Add(new CDACodifCode("code", "SOCANAM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Анамнез"));
            elements.Add(new CDATitle("АНАМНЕЗ"));
            // Менеджер текстовых ссылок. Создет текстовые ссылки с последовательно увеличивающимися номерами
            CDATextReferenceManager mngRef = new CDATextReferenceManager("socanam");
            // Текстовое наполнение секции
            CDAText text = new CDAText();
            elements.Add(text);
            // Добавим первым список с занятостью. Заполним его позже
            CDATextList lstJob = text.AddList("Занятость", new CDAElementAttribute("listType", "unordered"));
            // Список льготных категорий
            List<CDACodeTime> priveleges = new List<CDACodeTime>();
            priveleges.Add(new CDACodeTime(new CDACodifCode("code", "9752", "1.2.643.5.1.13.13.11.1050", "Льготные категории населения", "Почетные доноры"),
                                            new CDATime(new DateTime(2016, 02, 02), new DateTime(2017, 02, 02))));
            priveleges.Add(new CDACodeTime(new CDACodifCode("code", "6731", "1.2.643.5.1.13.13.11.1050", "Льготные категории населения", "Сотрудники подразделений особого риска"),
                                            new CDATime(new DateTime(2015, 02, 02), new DateTime(2016, 12, 12))));
            // Заполним список с льготными категориями и добавим их в документ           
            CDATextList lstPriv = text.AddList("Льготная категория", new CDAElementAttribute("listType", "unordered"), new CDAElementAttribute("ID", "priveleges"));            
            foreach (CDACodeTime privelege in priveleges)
            {
                // Создадим новую текстовую ссылку
                CDATextReference reference = mngRef.GetTextReference();
                // Добавим ее в льготную категорию
                privelege.SetCodeReference(reference);
                // Название льготной категории privelege.Name будет обрамлено в <content> со ссылкой
                lstPriv.AddItem(reference.GetContentXml(privelege.Name) + " (до " + privelege.HighDate + ")");
                // Добавим льготу в документ
                elements.Add(new CDAContainer("entry", privelege));
            }

            // Инвалидность
            // Время установления инвалидности
            CDATime disabilityTime = new CDATime(new DateTime(1961, 03, 31), default(DateTime)); // default(DateTime) - аналог null
            // Порядок установления инвалидности
            CDACodifCode disabilityOrder = new CDACodifCode("value", "1", "1.2.643.5.1.13.13.11.1041", "Порядок установления инвалидности", "Впервые");
            // Категория инвалидности: CDACodifCode, содержащий CDAQualifier
            CDACodifCode disabilityType = new CDACodifCode("code", "9313", "1.2.643.5.1.13.13.11.1050", "Льготные категории населения", "Инвалиды I группы",
                                                            new CDAQualifier(disabilityOrder));
            // Инвалидность
            CDACodeTime disability = new CDACodeTime(disabilityType, disabilityTime);
            // Добавим в текстовую часть
            CDATextList lstDis = text.AddList("Инвалидность", new CDAElementAttribute("listType", "unordered"), new CDAElementAttribute("ID", "invalid"));
            CDATextReference refDis = mngRef.GetTextReference();
            lstDis.AddItem(refDis.GetContentXml(disability.Name) + " с " + disability.LowDate);
            // Добавим в документ
            elements.Add(new CDAContainer("entry", disability));

            // Список потенциально опасных для здоровья соц. факторов и провессиональных вредностей
            List<CDACodifCode> badFactors = new List<CDACodifCode>();
            badFactors.Add(new CDACodifCode("code", "9752", "1.2.643.5.1.13.13.11.1059", "Потенциально-опасные для здоровья социальные факторы", "Нарушение режима сна и бодрствования"));
            badFactors.Add(new CDACodifCode("code", "589", "1.2.643.5.1.13.13.11.1060", "Профессиональные вредности для учета сигнальной информации о пациенте", "Тяжелый и напряженный физический труд"));
            CDATextList lstBadFactors = text.AddList("Социальные факторы и профессиональные вредности", new CDAElementAttribute("listType", "unordered"));
            foreach (CDACodifCode badFactor in badFactors)
            {
                CDATextReference reference = mngRef.GetTextReference();
                badFactor.SetReference(reference);
                lstBadFactors.AddItem(reference.GetContentXml(badFactor.GetDisplayName()));
                elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(badFactor)));
            }

            // Местность регистрации
            elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(
                                new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1042", "Признак жителя города или села", "Город"))));

            // Занятость
            CDACodeText job = new CDACodeText(new CDACodifCode("5", "1.2.643.5.1.13.13.11.1038", "Занятость (социальные группы) населения", "Работает"),
                                              new CDAText("Работает в «ООО Организация» в должности «Менеджер»"));
            CDATextReference refJob = mngRef.GetTextReference();
            job.SetReference(refJob);
            lstJob.AddItem(refJob.GetContentXml(job.Text));
            elements.Add(new CDAContainer("entry", job));

            // Список вредных привычек
            List<CDACodifCode> addictions = new List<CDACodifCode>();
            addictions.Add(new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1058", "Вредные привычки и зависимости", "Курение"));
            addictions.Add(new CDACodifCode("code", "9", "1.2.643.5.1.13.13.11.1058", "Вредные привычки и зависимости", "Телевизионная зависимость"));
            addictions.Add(new CDACodifCode("code", "10", "1.2.643.5.1.13.13.11.1058", "Вредные привычки и зависимости", "Интернет-зависимость"));
            CDATextList lstAddictions = text.AddList("Зависимости");
            CDATextReferenceManager mngAddictions = new CDATextReferenceManager("addic");
            foreach (CDACodifCode addiction in addictions)
            {
                CDATextReference reference = mngAddictions.GetTextReference();
                addiction.SetReference(reference);
                lstAddictions.AddItem(reference.GetContentXml(addiction.GetDisplayName()));
                elements.Add(new CDAContainer("entry", CDAActStatement.CreateObservation(addiction)));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionAMBSV()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            // Код секции
            elements.Add(new CDACodifCode("code", "AMBSV", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Сведения амбулаторно-поликлинического посещения"));
            elements.Add(new CDATitle("ПОСЕЩЕНИЯ"));
            // Текстовое наполнение секции
            CDAText text = new CDAText();
            elements.Add(text);            
            // Посещения
            List<CDAVisit> visits = new List<CDAVisit>();
            visits.Add(CDAVisit.Create());
            // Заполним текстовую часть и добавим посещения в документ
            CDATextTable tblVisit = text.AddTable(null, 5, 10, 15, 30, 15, 20);
            tblVisit.AddHeader("Дата", "Врач", "Жалобы пациента", "Объективно", "Заключение", "Рекомендовано");
            foreach (CDAVisit visit in visits)
            {
                tblVisit.AddRow(visit.Date, visit.Performer, visit.Complaint, visit.ObjectiveData, visit.Conclusion, visit.Recomendation);
                elements.Add(new CDAContainer("entry", visit));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionALL()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "ALL", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Аллергии и непереносимость"));
            elements.Add(new CDATitle("ПАТОЛОГИЧЕСКИЕ РЕАКЦИИ"));
            // Список аллергических реакций
            List<CDAAllergy> lstAllergy = new List<CDAAllergy>();
            lstAllergy.Add(CDAAllergy.Create(""));
            lstAllergy.Add(CDAAllergy.Create("Другая аллергическая реакция"));
            // Текстовое наполнение секции
            CDAText text = new CDAText();
            elements.Add(text);
            CDATextTable table = text.AddTable(null, 10, 10, 20, 20, 40);
            table.AddHeader("Дата выявления", "Тип агента", "МНН/Агент", "Тип реакции", "Комментарии");
            // Менеджер текстовых ссылок
            CDATextReferenceManager mngAllergen = new CDATextReferenceManager("allc");
            CDATextReferenceManager mngReaction = new CDATextReferenceManager("allr");
            foreach (CDAAllergy allergy in lstAllergy)
            {
                CDATextReference refAllergen = mngAllergen.GetTextReference();
                CDATextReference refReaction = mngReaction.GetTextReference();
                allergy.Allergen.AddTextReference(refAllergen);
                allergy.Reaction.AddTextReference(refReaction);
                table.AddRow(allergy.Date,
                             allergy.Allergen.Type,
                             refAllergen.GetContentXml(allergy.Allergen.Agent),
                             allergy.Reaction.Type + "<br/>(" + refReaction.GetContentXml(allergy.Type) + ")",
                             allergy.Comment);
                // Добавим описание аллергии в список
                elements.Add(new CDAContainer("entry", allergy));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionVITALPARAM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "VITALPARAM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Витальные параметры"));
            elements.Add(new CDATitle("ВИТАЛЬНЫЕ ПАРАМЕТРЫ"));
            // Список измерений витальных параметров
            List<CDAVitalParameterMeasuring> observations = new List<CDAVitalParameterMeasuring>();
            observations.Add(CDAVitalParameterMeasuring.CreatePressure());
            observations.Add(CDAVitalParameterMeasuring.CreatePulse());
            observations.Add(CDAVitalParameterMeasuring.CreateBreathRate());
            // Текстовое наполнение секции
            CDAText text = new CDAText();
            elements.Add(text);
            CDATextTable table = text.AddTable(null, 60, 10, 15, 15);
            table.AddHeader("Параметр", "Ед.Изм.", "Время", "Значение");
            // Заполним таблицу данными измерений
            CDATextReferenceManager refMan = new CDATextReferenceManager("VIT_");
            foreach (CDAVitalParameterMeasuring observation in observations)
            {
                string val = "";
                foreach (CDAVitalParameter parameter in observation.VitalParameters)
                {
                    CDATextReference reference = refMan.GetTextReference();
                    parameter.AddTextReference(reference);
                    val = CDAString.ConcatWithSeparator(val, reference.GetContentXml(parameter.Value.Value), " / ");                    
                }
                table.AddRow(observation.Name, observation.Unit, observation.Time, val);

                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionPROC()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "PROC", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Исследования и процедуры"));
            elements.Add(new CDATitle("Диагностические исследования и консультации"));
            // Помещаем секции в элемент <component>
            elements.Add(new CDAContainer("component", CreateSectionRESINSTR()));
            elements.Add(new CDAContainer("component", CreateSectionRESLAB()));
            elements.Add(new CDAContainer("component", CreateSectionRESMOR()));
            elements.Add(new CDAContainer("component", CreateSectionRESCONS()));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESINSTR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESINSTR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты инструментальных исследований"));
            elements.Add(new CDATitle("Результаты инструментальных исследований"));
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            // Исследования
            List<CDAObservationInstr> lstObs = new List<CDAObservationInstr>();
            lstObs.Add(CDAObservationInstr.CreateAmb());
            // Добавим в текстовый документ таблицу с исследованиями
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Исполнитель");
            // Пройдемся по исследованиям и добавим инфу в текстовое наполнение
            foreach (CDAObservationInstr observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Performer);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESLAB()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESLAB", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты лабораторных исследований"));
            elements.Add(new CDATitle("Результаты лабораторных исследований"));
            // текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);            
            // список с исследованиями
            List<CDALabObservation> observations = new List<CDALabObservation>();
            observations.Add(CDALabObservation.Create("id исследования"));
            // Признак первого исследования
            bool isFirst = true;
            foreach (CDALabObservation observation in observations)
            {
                // Добавим разрыв строки, если исследование не первое
                if (!isFirst)
                    text.AddBr();
                isFirst = false;
                // таблица с результатами исследования
                CDATextTable tblObs = text.AddTable(null, 22, 13, 10, 15, 12, 8, 17);
                tblObs.AddHeader("Показатель", "Значение", "Единицы измерения", "Референтный диапазон", "Оборудование",
                                    "Дата", "Исполнитель");
                // Заполним таблицу и пройдемся по группам лабораторных показателей
                foreach (CDALabFactorGroup factorGroup in observation.LabFactorGroups)
                {
                    tblObs.AddSingleRow(factorGroup.Name, new CDAElementAttribute("styleCode", "Bold"));
                    // пройдемся по лабораторным показателям
                    foreach (CDALabFactor factor in factorGroup.LabFactors)
                        tblObs.AddRow(factor.Name, factor.Value, factor.Unit, factor.Range, factor.Equipment, factor.Date, factor.Performer);
                }

                // добавим обследование в список элементов
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESMOR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESMOR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Результаты морфологических исследований"));
            elements.Add(new CDATitle("Результаты морфологических исследований"));
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            // Исследования
            List<CDAObservationMor> lstObs = new List<CDAObservationMor>();
            lstObs.Add(CDAObservationMor.CreateAmb());
            // Добавим в текстовый документ таблицу с исследованиями
            CDATextTable table = text.AddTable("Дата", "Исследование", "Результаты", "Исполнитель");
            // Пройдемся по исследованиям и добавим инфу в текстовое наполнение
            foreach (CDAObservationMor observation in lstObs)
            {
                table.AddRow(observation.Date, observation.Name, observation.Result, observation.Performer);
                elements.Add(new CDAContainer("entry", observation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionRESCONS()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "RESCONS", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Консультации врачей специалистов"));
            elements.Add(new CDATitle("Консультации врачей специалистов"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Список консультаций
            List<CDAConsultation> consultations = new List<CDAConsultation>();
            consultations.Add(CDAConsultation.Create());
            CDATextTable tblCons = text.AddTable("Дата", "Исследование", "Результаты", "Исполнитель");
            int i = 1;
            foreach (CDAConsultation consultation in consultations)
            {
                tblCons.AddRow(consultation.Time, consultation.Job, consultation.GenerateProtocol(new CDATextReferenceManager("RES_" + i + "_")), consultation.Performer);
                elements.Add(new CDAContainer("entry", consultation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionIMM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "IMM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Вакцинация и иммунизация"));
            elements.Add(new CDATitle("ВАКЦИНАЦИЯ И ИММУНИЗАЦИЯ"));
            // Список вакцинаций
            List<CDAVaccination> vaccinations = new List<CDAVaccination>();
            vaccinations.Add(CDAVaccination.Create());
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            CDATextTable table = text.AddTable("Дата", "Препарат", "Комментарий");
            // Менеджер текстовых ссылок
            CDATextReferenceManager refMan = new CDATextReferenceManager("imm");
            foreach (CDAVaccination vac in vaccinations)
            {
                // Новая уникальная текстовая ссылка
                CDATextReference reference = refMan.GetTextReference();
                // Добавим ее к вакцинации
                vac.AddTextReference(reference);
                // Выведем сторку в текстовую таблицу
                table.AddRow(vac.Date, reference.GetContentXml(vac.Vaccine.Name), vac.Comment);
                // Добавим вакцинацию в список элементов
                elements.Add(new CDAContainer("entry", vac));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSUM()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SUM", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Информация о лечении"));
            elements.Add(new CDATitle("Информация о лечении"));
            // Помещаем секции в элемент <component>
            elements.Add(new CDAContainer("component", CreateSectionDRUG()));
            elements.Add(new CDAContainer("component", CreateSectionNONDRUG()));

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionDRUG()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "DRUG", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Медикаменты"));
            elements.Add(new CDATitle("Медикаменты"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Список назначений лекарств
            List<CDADrugAdministration> administrations = new List<CDADrugAdministration>();
            administrations.Add(new CDADrugAdministration(
                        new CDADrug(new CDACodifCode("code", "218", "1.2.643.5.1.13.2.1.1.179", "Классификатор международных непатентованных наименований лекарственных средств", "Ацетилсалициловая кислота")),
                        new CDAText("2 таб. в день"),
                        new CDATime(new DateTime(2016, 10, 02), new DateTime(2016, 10, 15))));
            CDATextTable tblDrugs = text.AddTable(null, 55, 20, 25);
            tblDrugs.AddHeader("МНН", "Схема лечения", "Период лечения");
            foreach (CDADrugAdministration administration in administrations)
            {
                tblDrugs.AddRow(administration.Name, administration.Scheme, administration.Time);
                elements.Add(new CDAContainer("entry", administration.Drug));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionNONDRUG()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "NONDRUG", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Немедикаментозное лечение"));
            elements.Add(new CDATitle("Немедикаментозное лечение"));
            CDAText text = new CDAText("Прикладывать подорожник");
            elements.Add(text);

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSUR()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SUR", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Хирургические операции"));
            elements.Add(new CDATitle("ХИРУРГИЧЕСКИЕ ВМЕШАТЕЛЬСТВА"));
            CDAText text = new CDAText();
            elements.Add(text);
            // Список операций
            List<CDAOperation> operations = new List<CDAOperation>();
            operations.Add(CDAOperation.CreateAmb());
            // Заполним таблицу с операциями и добавим их в документ
            CDATextTable tbl = text.AddTable(null, 10, 10, 55, 15, 10);
            tbl.AddHeader("Дата", "Код", "Название", "Вид анестезии", "Хирург");
            foreach (CDAOperation operation in operations)
            {
                tbl.AddRow(operation.Date, operation.Code, operation.Name, operation.Anesthesy, operation.Performer);
                elements.Add(new CDAContainer("entry", operation));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionPROCEDURE()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "PROCEDURE", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Медицинские процедуры"));
            elements.Add(new CDATitle("МЕДИЦИНСКИЕ ПРОЦЕДУРЫ"));
            // Список процедур
            List<CDAProcedure> procedures = new List<CDAProcedure>();
            procedures.Add(CDAProcedure.Create());
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            CDATextTable table = text.AddTable(null, 10, 10, 55, 15, 10);
            table.AddHeader("Дата", "Код", "Название", "Вид анестезии", "Врач");
            foreach (CDAProcedure procedure in procedures)
            {
                table.AddRow(procedure.Date, procedure.Code, procedure.Name + "<br/>" + "<content styleCode=\"Italics\">установлено:</content> " + procedure.Device,
                             procedure.Type, procedure.Performer);
                elements.Add(new CDAContainer("entry", procedure));
            }

            return new CDADocumentSection(elements);
        }

        public static CDADocumentSection CreateSectionSERVICES()
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(new CDACodifCode("code", "SERVICES", "1.2.643.5.1.13.2.1.1.1504.23", "Справочник секций документов", "Оказанные медицинские услуги"));
            elements.Add(new CDATitle("МЕДИЦИНСКИЕ УСЛУГИ"));
            // Создадим список услуг
            List<CDAService> services = new List<CDAService>();
            services.Add(new CDAService(new CDACodifCode("code", "A05.10.008.001", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Холтеровское мониторирование сердечного ритма (ХМ-ЭКГ)"),
                                        new CDATime(new DateTime(2016, 08, 20, 08, 08, 00, DateTimeKind.Local))));
            services.Add(new CDAService(new CDACodifCode("code", "B03.016.002", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Общий (клинический) анализ крови"),
                                        new CDATime(new DateTime(2016, 09, 02, 11, 11, 00, DateTimeKind.Local))));
            services.Add(new CDAService(new CDACodifCode("code", "A12.05.005", "1.2.643.5.1.13.13.11.1070", "Номенклатура медицинских услуг", "Определение основных групп крови (A, B, 0)"),
                                        new CDATime(new DateTime(2016, 09, 02, 12, 12, 00, DateTimeKind.Local))));
            // Текстовое наполнение
            CDAText text = new CDAText();
            elements.Add(text);
            CDATextTable table = text.AddTable(null, 10, 80, 10);
            table.AddHeader("Дата", "Услуга", "Код");
            // Пройдемся по списку услуг
            foreach (CDAService service in services)
            {
                table.AddRow(service.Date, service.Name, service.Code);
                elements.Add(new CDAContainer("entry", service));
            }

            return new CDADocumentSection(elements);
        }
    }
}
