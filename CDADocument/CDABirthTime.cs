﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Время рождения
    public class CDABirthTime : ICDASerializable
    {
        private CDATime birthday;

        public CDABirthTime(DateTime birthday)
        {
            this.birthday = new CDATime(birthday, "birthTime");
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            return birthday.ToXmlElement(document);
        }
    }
}
