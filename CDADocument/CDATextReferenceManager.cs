﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDADocumentClasses
{    
    // генератор ссылок CDATextReference  
    // создает ссылки с именем, начинающимся на prefix, и увеличивающимися номерами
    public class CDATextReferenceManager
    {
        private string prefix;
        private int num;

        public CDATextReferenceManager(string prefix)
        {
            this.prefix = prefix;
            num = 0;
        }

        public CDATextReference GetTextReference()
        {
            num = num + 1;
            return new CDATextReference(prefix + num.ToString());
        }

        public void Reset()
        {
            num = 0;
        }
    }
}
