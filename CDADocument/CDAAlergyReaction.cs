﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Аллергическая реакция
    public class CDAAlergyReaction : ICDASerializable
    {
        private CDACodifCode code;

        public string Type
        { get { return code.GetDisplayName(); } }

        public CDAAlergyReaction(CDACodifCode code)
        {
            this.code = code; // реакция
        }

        public void AddTextReference(CDATextReference reference)
        {
            code.AddComponent(reference);
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer reaction = new CDAContainer("entryRelationship", "MFST", CDAActStatement.CreateObservation(code));
            return reaction.ToXmlElement(document);
        }
    }
}
