﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Описания случая оказания мед. помощи из заголовка документа
    public class CDAEncompassingEncounter : ICDASerializable
    {
        // Уникальный идентифиактор
        private CDARootExt Id;
        // История болезни
        private CDARootExt History;
        // Тип случая мед. помощи
        private CDACodifCode Type;
        // Даты поступления и выписки
        private CDATime Period;
        // Исход
        private CDACodifCode Outcome;

        public CDAEncompassingEncounter(CDARootExt Id, CDARootExt History, CDACodifCode Type, CDATime Period, CDACodifCode Outcome)
        {
            this.Id = Id;
            this.History = History;
            this.Type = Type;
            this.Period = Period;
            this.Outcome = Outcome;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            XmlElement elEncounter = document.CreateElement("componentOf");
            XmlElement elEncomp = document.CreateElement("encompassingEncounter");
            elEncomp.AppendChild(Id.ToXmlElementAtr(document.CreateElement("id")));
            elEncomp.AppendChild(History.ToXmlElementAtr(document.CreateElement("id")));
            elEncomp.AppendChild(Type.ToXmlElementAtr(document.CreateElement("code")));
            XmlElement elTime = Period.ToXmlElement(document);
            elEncomp.AppendChild(elTime);
            elEncomp.AppendChild(Outcome.ToXmlElementAtr(document.CreateElement("dischargeDispositionCode")));

            elEncounter.AppendChild(elEncomp);

            return elEncounter;
        }

        public static CDAEncompassingEncounter Create()
        {
            CDARootExt Id = new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.15", "908964234678");
            CDARootExt History = new CDARootExt("1.2.643.5.1.13.3.25.77.923.100.1.1.16", "8895\\14");
            CDACodifCode Type = new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.1504.22", "Тип случая оказания медицинской помощи", "Госпитализация");
            CDATime Period = new CDATime(new DateTime(2014, 05, 13, 11, 15, 0, DateTimeKind.Local), new DateTime(2014, 05, 23, 15, 45, 0, DateTimeKind.Local));
            CDACodifCode Outcome = new CDACodifCode("1", "1.2.643.5.1.13.2.1.1.1504.2", "Классификатор исходов госпитализации", "выписан");

            return new CDAEncompassingEncounter(Id: Id, History: History, Type: Type, Period: Period, Outcome: Outcome);
        }
    }
}
