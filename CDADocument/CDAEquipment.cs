﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Тип использованной аппаратуры 
    public class CDAEquipment : ICDASerializable
    {
        private CDACodifCode code;

        public CDACodifCode Code
        { get { return code; } }

        public CDAEquipment(CDACodifCode code)
        {
            this.code = code;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            CDAContainer container = new CDAContainer("participant", "DEV",
                                         new CDAContainer("participantRole",
                                             new CDAContainer("playingDevice", code),
                                             new CDAElementAttribute("classCode", "MNT")));
            return container.ToXmlElement(document);
        }
    }
}
