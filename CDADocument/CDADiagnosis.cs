﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Диагноз (Первичный, заключительный итд)
    // Может включать в себя несоклько CDADiagnosisInfo
    public class CDADiagnosis : ICDASerializable
    {
        private CDACodifCode DiagType; // степень обоснованности диагноза
        private List<CDADiagnosisInfo> components; // список диагнозов (основной, сопутствующий и т.д.)

        public string Type
        { get { return DiagType.GetDisplayName(); } }

        public List<CDADiagnosisInfo> Components
        { get { return components; } }

        public CDADiagnosis(CDACodifCode DiagType, List<CDADiagnosisInfo> Components)
        {
            this.DiagType = DiagType;
            this.components = Components;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            // Список с наполнением диагноза
            List<ICDASerializable> elLst = new List<ICDASerializable>();
            elLst.Add(DiagType);
            foreach (CDADiagnosisInfo component in Components)
            {
                elLst.Add(new CDAContainer("entryRelationship", component, new CDAElementAttribute("typeCode", "COMP")));
            }
            // Создаем элемент <act>, заполненный элементами из elLst
            CDAActStatement act = CDAActStatement.CreateAct(elLst);

            return act.ToXmlElement(document);
        }

        // создает заключительный диагноз
        public static CDADiagnosis CreateFinal()
        {
            CDACodifCode DiagType = new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1076", "Степень обоснованности диагноза", "Заключительный диагноз");
            List<CDADiagnosisInfo> lstDiag = new List<CDADiagnosisInfo>();
            // основной диагноз
            lstDiag.Add(new CDADiagnosisInfo(
                NosolEntity: new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1077", "Виды нозологических единиц диагноза", "Основное заболевание"),
                Description: new CDAText("Термический ожог запястья и кисти второй степени"),
                Diagnosis: new CDACodifCode("code", "T23.2", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Термический ожог запястья и кисти второй степени"),
                ExtReason: new CDACodifCode("code", "X11", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Соприкосновение с горячей водой из крана"),
                TraumaSpec: new CDACodifCode("code", "14", "1.2.643.5.1.13.2.1.1.105", "Виды травм по способу получения", "Прочие"),
                DiagSpec: new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1049", "Характер заболевания", "Острое"),
                DispStatus: new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1047", "Статусы диспансерного наблюдения", "Снят"),
                DispSpec: new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1045", "Причины снятия с диспансерного учета", "Выздоровление")
                ));
            // сопутствующая патология
            lstDiag.Add(new CDADiagnosisInfo(
                NosolEntity: new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1077", "Виды нозологических единиц диагноза", "Сопутствующая патология"),
                Description: new CDAText("Простой хронический бронхит"),
                Diagnosis: new CDACodifCode("code", "J41.0", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Простой хронический бронхит")
                ));
            // еще сопутствующая патология
            lstDiag.Add(new CDADiagnosisInfo(
                NosolEntity: new CDACodifCode("code", "3", "1.2.643.5.1.13.13.11.1077", "Виды нозологических единиц диагноза", "Сопутствующая патология"),
                Description: new CDAText("Термический ожог запястья и кисти второй степени"),
                Diagnosis: new CDACodifCode("code", "N30.1", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Интерстициальный цистит (хронический)")
                ));

            return new CDADiagnosis(DiagType, lstDiag);
        }

        // создает предварительный диагноз
        public static CDADiagnosis CreatePre()
        {
            CDACodifCode DiagType = new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1076", "Степень обоснованности диагноза", "Предварительный диагноз");
            List<CDADiagnosisInfo> lstDiag = new List<CDADiagnosisInfo>();
            lstDiag.Add(new CDADiagnosisInfo(
                NosolEntity: new CDACodifCode("code", "1", "1.2.643.5.1.13.13.11.1077", "Виды нозологических единиц диагноза", "Основное заболевание"),
                Description: new CDAText("Термический ожог запястья и кисти второй степени"),
                Diagnosis: new CDACodifCode("code", "T23.2", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Термический ожог запястья и кисти второй степени"),
                ExtReason: new CDACodifCode("code", "X11", "1.2.643.5.1.13.13.11.1005", "Международная классификация болезней и состояний, связанных со здоровьем 10 пересмотра. Версия 4", "Соприкосновение с горячей водой из крана"),
                TraumaSpec: null, DiagSpec: null, DispStatus: null, DispSpec: null
                ));

            return new CDADiagnosis(DiagType, lstDiag);
        }
    }
}
