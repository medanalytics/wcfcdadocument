﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CDADocumentClasses
{
    // Единица информации о диагнозе (основной, сопутствующий итд)
    public class CDADiagnosisInfo : ICDASerializable
    {
        private CDACodifCode NosolEntity; // нозологическая единица
        private CDAText Description; // врачебное описание
        private CDACodifCode Diagnosis; // код основного заболевания по МКБ
        private CDAValue ValDiagnosis; // код основного заболевания по МКБ (элемент value)
        private CDACodifCode ExtReason; // внешняя причина
        private CDACodifCode TraumaSpec; // уточнение вида травмы
        private CDACodifCode DiagSpec; // уточнение характера заболевания
        private CDACodifCode DispStatus; // статус диспансерного наблюдения
        private CDACodifCode DispSpec; // уточнение причины снятия с диспансерного учета        

        public string Code
        { get { return Diagnosis.GetCode(); } }

        public string Type
        { get { return NosolEntity.GetDisplayName(); } }

        public string Name
        { get { return Diagnosis.GetDisplayName(); } }

        public string Text
        {
            get
            {
                string val = Diagnosis.GetDisplayName();
                if (ExtReason != null)
                    val = CDAString.ConcatWithSeparator(val, "внешняя причина: " + ExtReason.GetDisplayName(), "<br/>");
                if (TraumaSpec != null)
                    val = CDAString.ConcatWithSeparator(val, "травма: " + TraumaSpec.GetDisplayName(), "<br/>");
                if (DiagSpec != null)
                    val = CDAString.ConcatWithSeparator(val, "заболевание: " + DiagSpec.GetDisplayName(), "<br/>");
                if (DispStatus != null)
                    val = CDAString.ConcatWithSeparator(val, "диспансерное наблюдение: " + DispStatus.GetDisplayName(), "<br/>");
                return val;
            }
        }

        public CDADiagnosisInfo(CDACodifCode NosolEntity, CDAText Description, CDACodifCode Diagnosis, CDACodifCode ExtReason,
                                CDACodifCode TraumaSpec, CDACodifCode DiagSpec, CDACodifCode DispStatus, CDACodifCode DispSpec)
        {
            this.NosolEntity = NosolEntity;
            this.Description = Description;
            this.Diagnosis = Diagnosis;
            this.ValDiagnosis = new CDAValue(Diagnosis);
            this.ExtReason = ExtReason;
            this.TraumaSpec = TraumaSpec;
            this.DiagSpec = DiagSpec;
            this.DispStatus = DispStatus;
            this.DispSpec = DispSpec;
        }

        public CDADiagnosisInfo(CDACodifCode NosolEntity, CDAText Description, CDACodifCode Diagnosis)
        {
            this.NosolEntity = NosolEntity;
            this.Description = Description;
            this.Diagnosis = Diagnosis;
            this.ValDiagnosis = new CDAValue(Diagnosis);
            this.ExtReason = null;
            this.TraumaSpec = null;
            this.DiagSpec = null;
            this.DispStatus = null;
            this.DispSpec = null;
        }

        public CDADiagnosisInfo(CDACodifCode NosolEntity, CDACodifCode Diagnosis)
        {
            this.NosolEntity = NosolEntity;
            this.Description = null;
            this.Diagnosis = Diagnosis;
            this.ValDiagnosis = new CDAValue(Diagnosis);
            this.ExtReason = null;
            this.TraumaSpec = null;
            this.DiagSpec = null;
            this.DispStatus = null;
            this.DispSpec = null;
        }

        public XmlElement ToXmlElement(XmlDocument document)
        {
            List<ICDASerializable> elements = new List<ICDASerializable>();
            elements.Add(NosolEntity);
            if (Description != null)
                elements.Add(Description);
            elements.Add(ValDiagnosis);
            if (ExtReason != null)
                elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateAct(ExtReason), 
                                    new CDAElementAttribute("typeCode", "CAUS"), new CDAElementAttribute("inversionInd", "true")));
            if (TraumaSpec != null)
                elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateAct(TraumaSpec),
                                    new CDAElementAttribute("typeCode", "REFR"), new CDAElementAttribute("inversionInd", "false")));
            if (DiagSpec != null)
                elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateAct(DiagSpec),
                                    new CDAElementAttribute("typeCode", "SUBJ"), new CDAElementAttribute("inversionInd", "true")));
            if (DispStatus != null)
            {
                List<ICDASerializable> lstDisp = new List<ICDASerializable>();
                lstDisp.Add(DispStatus);
                if (DispSpec != null)
                    lstDisp.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateAct(DispSpec),
                                        new CDAElementAttribute("typeCode", "RSON"), new CDAElementAttribute("inversionInd", "false")));

                elements.Add(new CDAContainer("entryRelationship", CDAActStatement.CreateAct(lstDisp),
                                    new CDAElementAttribute("typeCode", "SUBJ"), new CDAElementAttribute("inversionInd", "true")));
            }

            return CDAActStatement.CreateObservation(elements).ToXmlElement(document);            
        }
    }
}
