﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using CDADocumentClasses;
using CDATools;

namespace CDATest
{
    class Program
    {
        static void Main(string[] args)
        {            
            CDADocument doc = CDADocumentAmbulatorySummary.Create();
            XmlDocument document = doc.ToXmlDocument();
                        
            FileStream fs = File.Create("test.xml");
            XmlTextWriter writer = new XmlTextWriter(fs, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            document.WriteTo(writer);
            writer.Flush();
            fs.Flush();
            fs.Close();
        }        
    }
}
