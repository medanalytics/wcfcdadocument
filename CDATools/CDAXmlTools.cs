﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;

namespace CDATools
{
    // Класс для валидации и преобразования XML-документов
    public static class CDAXmlTools
    {
        // Получение адреса схемы
        public static string GetXmlDocSchemaLocation(XmlDocument document)
        {
            XmlElement root = document.DocumentElement;
            XmlNode schemaLocationAttribute = root.SelectSingleNode("//@*[local-name()='schemaLocation']");
            string[] components = schemaLocationAttribute.Value.Split(null);
            return components[1];
        }
        // Получение пространства имен
        public static string GetXmlDocNamespaceURN(XmlDocument document)
        {
            XmlElement root = document.DocumentElement;
            XmlNode schemaLocationAttribute = root.SelectSingleNode("//@*[local-name()='schemaLocation']");
            string[] components = schemaLocationAttribute.Value.Split(null);
            return components[0];
        }
        // Получение ссылки на stylesheet
        public static string GetXmlDocStylesheetHref(XmlDocument document)
        {
            string href = null;
            XmlProcessingInstruction instruction = document.SelectSingleNode("processing-instruction('xml-stylesheet')") as XmlProcessingInstruction;
            string[] parameters = instruction.InnerText.Split(null);
            foreach (string param in parameters)
            {
                if (param.Substring(0, 4) == "href")
                {
                    string[] values = param.Split(new char[] { '=' });
                    href = values[1].Replace("\"", "");
                    break;
                }
            }
            return href;
        }
        // xsl-преобразование документа по его stylesheet'у
        public static string TransformXmlDocToHtml(XmlDocument document)
        {
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(CDAXmlTools.GetXmlDocStylesheetHref(document));

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);

            transform.Transform(document, xw);

            return sw.ToString();
        }
        // валидация xml-документа по схеме, которая в нем указана
        public static bool IsValidXmlDoc(XmlDocument document, out string messages)
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder();            
            document.Schemas.Add(GetXmlDocNamespaceURN(document), GetXmlDocSchemaLocation(document));
            ValidationEventHandler handler = new ValidationEventHandler(ValidationEventHandler);
            try
            {
                document.Validate(handler);
            }
            catch (XmlSchemaValidationException e)
            {
                sb.AppendLine("--Ошибка-----");
                sb.AppendLine(e.Message);
                isValid = false;
            }
            messages = sb.ToString();
            return isValid;
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            throw e.Exception;
        }
    }
}
