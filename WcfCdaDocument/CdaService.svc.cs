﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using CDADocumentClasses;

namespace WcfCdaDocument
{
    public class CdaService : ICdaService
    {
        public string CreateDischargeSummary()
        {
            CDADocument medDoc = CDADocumentDischargeSummary.Create();
            XmlDocument document = medDoc.ToXmlDocument();
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            document.WriteTo(tw);

            return sw.ToString();
        }

        public string CreateAmbSummary()
        {            
            CDADocument medDoc = CDADocumentAmbulatorySummary.Create();
            XmlDocument document = medDoc.ToXmlDocument();
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            document.WriteTo(tw);

            return sw.ToString();
        }
    }
}