﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.ServiceModel;
using CDATools;

namespace CdaClient
{
    public partial class Form1 : Form
    {
        private XmlDocument document;

        public Form1()
        {
            InitializeComponent();
            cbxReports.SelectedIndex = 0;            
        }

        private void ValidateAndDisplay()
        {
            // Проведем валидацию
            string messages;
            CDAXmlTools.IsValidXmlDoc(document, out messages);
            // Выведем ошибки валидации
            txtErrors.Clear();
            txtErrors.AppendText(messages);
            // Проведем xsl-преобразование и выведем результат в браузер
            webBrowser.DocumentText = CDAXmlTools.TransformXmlDocToHtml(document);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {            
            // Создаем клиент WCF-сервиса и получаем документ в виде string
            CdaService.CdaServiceClient client = new CdaService.CdaServiceClient();
            string sdoc;
            if (cbxReports.SelectedItem.ToString() == "Амбулаторный эпикриз")
                sdoc = client.CreateAmbSummary();
            else
                sdoc = client.CreateDischargeSummary();

            // Загружаем документ из string
            document = new XmlDocument();            
            document.LoadXml(sdoc);

            ValidateAndDisplay();
        }

        private void btnSave_Click(object sender, EventArgs e)         
        {            
            // Сохранение документа в файл
            FileStream fs;
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if ((fs = dialog.OpenFile() as FileStream) != null)
                {
                    XmlTextWriter writer = new XmlTextWriter(fs, Encoding.UTF8);
                    writer.Formatting = Formatting.Indented;
                    document.WriteTo(writer);
                    writer.Flush();                    
                    fs.Flush();
                    fs.Close();                    
                }
            }            
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Stream fs;
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((fs = dialog.OpenFile()) != null)
                    {
                        using (fs)
                        {
                            document = new XmlDocument();
                            document.Load(fs);
                            ValidateAndDisplay();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка: не прочитать файл. Оригинальная ошибка: " + ex.Message);
                }
            }
        }
    }
}
