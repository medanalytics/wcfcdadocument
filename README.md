### Тестовый WCF-сервис для формирования СЭМД ###

## Состав:

* WcfCdaDocument - WCF-сервис для формирования СЭМД;
* CdaClient - клиент. Получает документы и выводит их на экран в преобразованном для чтения виде;
* CDADocument - классы для формирования СЭМД;
* CDATools - класс для валидации и xsl-преобразования xml-документов;
* CDATest - консольное приложения для тестирования классов при разработке.

Запускать надо CdaClient.

## Описание

Сервис формирует 2 документа: Амбулаторный эпикриз и Выписной эпикриз.

Документы заполняются данными, прописанными в тексте программы. Поэтому методам, создающим объекты, в основном не передаются параметры.

В рабочих условиях данные должны браться из БД. В этом случае для создания объектов должны передаваться идентификаторы случая, услуги, диагноза и т.д.
